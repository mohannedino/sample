package com.mohanned.migration;


public class Criteria {
    String name;
    String owner;
    String criteriaPrtiority;
    String valueWithHighestWeight;
    String valueWithLowestWeight;

    public String getName() {
        return name;
    }

     public String getRefinedName() {
        return this.getName().replace("^^<http://www.w3.org/2001/XMLSchema#string>", "").replaceAll("\"", "");
    }
     
    public void setName(String name) {
        this.name = "\"" + name.replace("^^http://www.w3.org/2001/XMLSchema#string", "\"^^<http://www.w3.org/2001/XMLSchema#string>");
    }


    public String getOwner() {
        return owner;
    }

    public String getRefinedOwner() {
        return owner.replace("^^<http://www.w3.org/2001/XMLSchema#string>", "").replaceAll("\"", "");
    }

    public void setOwner(String owner) {
        this.owner = "\"" + owner.replace("^^http://www.w3.org/2001/XMLSchema#string", "\"^^<http://www.w3.org/2001/XMLSchema#string>");
    }

    ;

    public String getCriteriaPrtiority() {
        return "\"" + criteriaPrtiority.replace("^^http://www.w3.org/2001/XMLSchema#int", "\"^^<http://www.w3.org/2001/XMLSchema#int>");
    }

    public void setCriteriaPrtiority(String criteriaPrtiority) {
        this.criteriaPrtiority = criteriaPrtiority;
    }

    public String getRefinedCriteriaPrtiority() {
        return criteriaPrtiority.
                replace(Configuration.Namespace, "").
                replace("^^http://www.w3.org/2001/XMLSchema#int", "");
    }
    //public void setCriteriaPrtiority(String criteriaPrtiority){ this.criteriaPrtiority = criteriaPrtiority; }


    public void setValueWithHighestWeight(String valueWithHighestWeight) {
        this.valueWithHighestWeight = valueWithHighestWeight;
    }

    public String getValueWithHighestWeight() {
        return "\"" + valueWithHighestWeight.replace("^^http://www.w3.org/2001/XMLSchema#int", "\"^^<http://www.w3.org/2001/XMLSchema#int>");
    }

    public double getNumericalValueWithHighestWeight() {
        return Double.parseDouble(valueWithHighestWeight.substring(0, valueWithHighestWeight.indexOf("^")).replace("\"", ""));
    }


    public void setValueWithLowestWeight(String valueWithLowestWeight) {
        this.valueWithLowestWeight = valueWithLowestWeight;
    }

    public String getValueWithLowestWeight() {
        return "\"" + valueWithLowestWeight.replace("^^http://www.w3.org/2001/XMLSchema#int", "\"^^<http://www.w3.org/2001/XMLSchema#int>");
    }

    public double getNumericalValueWithLowestWeight() {
        return Double.parseDouble(valueWithLowestWeight.substring(0, valueWithLowestWeight.indexOf("^")).replace("\"", ""));
    }


}
