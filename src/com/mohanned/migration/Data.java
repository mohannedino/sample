package com.mohanned.migration;

import com.ws4d.droidcommander.SearchClient;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.security.SecurityKey;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.Service;
import org.ws4d.java.types.LocalizedString;
import org.ws4d.java.types.URI;

public class Data {

    public static URI getWs4dNameSpacedURI(String Name) {
        return new URI(Configuration.ws4dNamespace + Name);
    }

    public static String getPureServiceName(Service service) {
        return getShortServiceName(service).replace(Configuration.ws4dNamespace, "");
    }

    public static String getShortServiceName(Service service) {
        String serviceName = service.getServiceId().toString();
        if (serviceName.contains("-__WSDL__-")) {
            serviceName = serviceName.substring(0, service.getServiceId().toString().indexOf("-__WSDL__-"));
        }

        return serviceName;
    }

    public static String getServiceNameInModel(Service service) {
        return Configuration.Namespace + getShortServiceName(service);
    }

    public static int ordinalIndexOf(String str, char c, int n) {
        int pos = str.indexOf(c, 0);
        while (n-- > 0 && pos != -1) {
            pos = str.indexOf(c, pos + 1);
        }
        return pos;
    }

    public static String getDeviceIP(Device device) {
        /*if(device==null)
        {
            try {
                throw new Exception();
            } catch (Exception ex) {
                Logger.getLogger(Data.class.getName()).log(Level.SEVERE, null, ex);
            }
        }*/
        //SearchClient.deviceDetailList.(device)
        
        String IP = null;
        //IP = device.getDeviceReference(SecurityKey.EMPTY_KEY).getPreferredDiscoveryXAddressInfo().getXAddressAsString();
        org.ws4d.java.structures.Iterator x = device.getTransportXAddressInfos();

        while (x.hasNext()) //if (device.getTransportXAddressInfos().hasNext())
        {
            //HashMap xx = (HashMap) x.next();
            //devIP = (String) xx.get("host");
            String devIP = x.next().toString();
            if (devIP.substring(0, 20).contains("[")) {
                //devIP = devIP.substring(devIP.indexOf("["), devIP.indexOf("]") + 1);
                continue;
            } else if (!devIP.contains("10.0.")&& !devIP.contains("192.168.192")) {
                int start = devIP.indexOf("//") + 2;
                int end = devIP.indexOf(":", start);
                IP = devIP.substring(start, end);
                break;
                //, Data.ordinalIndexOf(devIP, ':', 1));
            }
        }

       // if(IP.contains("NULL"))
       //     getDeviceIP(device);
        String devName = Data.getShortDeviceName(device);

        if (devName.contains("X")) {
            IP = "192.168.56.1";
        } else if (devName.contains("Y")) {
            IP = "192.168.56.101";
        } else if (devName.contains("Z")) {
            IP = "192.168.56.102";
        }
        else if (devName.contains("M1")) {
            IP = "192.168.0.150";
        }
        else if (devName.contains("M2")) {
            IP = "192.168.0.45";
        }

        else if (devName.contains("D1")) {
            IP = "192.168.192.101";
        }else if (devName.contains("D2")) {
            IP = "192.168.192.102";
        }else if (devName.contains("D3")) {
            IP = "192.168.192.103";
        }else if (devName.contains("D4")) {
            IP = "192.168.192.104";
        }else if (devName.contains("D5")) {
            IP = "192.168.192.105";
        }

        return IP;
    }

    public static String getShortDeviceName(Device device) {
        if (!device.isValid()) {
            try {
                device.getDeviceReference(SecurityKey.EMPTY_KEY).rebuildDevice();
            } catch (CommunicationException ex) {
                Logger.getLogger(Data.class.getName()).log(Level.SEVERE, null, ex);
            }
          
        }
        String providerName = device.getFriendlyName((LocalizedString.LANGUAGE_EN));
        if (providerName.contains("-__IP__-")) {
            providerName = providerName.substring(0, providerName.indexOf("-__IP__-"));
        }
        return providerName;
    }

    public static String getDeviceNameInModel(Device device) {
        return Configuration.Namespace + getShortDeviceName(device);
    }

    public static double getDouble(String OntString) {
        return Double.parseDouble(OntString.replace("^^http://www.w3.org/2001/XMLSchema#int", "").toString());
    }
}
