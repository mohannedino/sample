package com.mohanned.migration;

import org.ws4d.java.service.Device;
import org.ws4d.java.service.Service;
import org.ws4d.java.service.reference.DeviceReference;


public class DevicesRec {

    private String ip;
    private String name;
    private DeviceReference device;

    /**
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip the ip to set
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the device
     */
    public DeviceReference getDevice() {
        return device;
    }

    /**
     * @param device the device to set
     */
    public void setDevice(DeviceReference device) {
        this.device = device;
    }
    
    

    
}
