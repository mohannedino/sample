package com.mohanned.migration;

import org.ws4d.java.service.DefaultDevice;
import org.ws4d.java.types.LocalizedString;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.ScopeSet;
import org.ws4d.java.types.ThisDeviceMData;

import java.text.SimpleDateFormat;
import java.util.Date;

public class IndividualProvider extends DefaultDevice {

    ScopeSet scopeList = null;

    public void addAttribute(QName attributeType, String attributeValue) {
        ThisDeviceMData s = this.getDeviceMetadata();
        s.addUnknownAttribute(attributeType, attributeValue);
        this.setDeviceMetadata(s);

    }
    
    public IndividualProvider() {
        super();

        // set PortType
        this.setPortTypes(new QNameSet(new QName("Provider", Configuration.ws4dNamespace)));

        addManufacturer(LocalizedString.LANGUAGE_EN, "VUT - Univ. Brno.");
        addModelName(LocalizedString.LANGUAGE_EN, "Provider");
        setModelNumber(System.getProperty("os.name"));
        setFirmwareVersion("v1.0");

        scopeList = new ScopeSet(new String[]{"urn:" + System.getProperty("os.name")});
        setScopes(scopeList);

        /*      Iterator itr = IPNetworkDetection.getInstance().getNetworkInterfaces();

        while (itr.hasNext()) {
            Log.d("IP", itr.next().toString());
        }
         */
        printInfo();

        /*NetworkInterface iface = IPNetworkDetection. getInstance(). getNetworkInterface ("eth0");
        IPDiscoveryDomain domain = IPNetworkDetection. getInstance().getIPDiscoveryDomainForInterface(iface, false );
        addBinding(new IPDiscoveryBinding(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID, domain));
        //addBinding (new HTTPBinding( IPNetworkDetection . getInstance ().getIPAddressOfAnyLocalInterface("127.0.0.1", false ), 0, " docuDevice ", DPWSCommunicationManager . COMMUNICATION_MANAGER_ID ));
        /**/
    }

    static String getCurrentTime(String format) throws IllegalArgumentException {
        if (format == null || format.length() == 0) {
            format = "HH:mm:ss:SSS";
        }
        SimpleDateFormat dateformat = new SimpleDateFormat(format);

        return dateformat.format(new Date());
    }

    ;

    public void printInfo() {
        System.err.println("DeviceInfo" + "Device info:");
        System.err.println("DeviceInfo" + "Device: " + System.getProperty("os.name"));
        System.err.println("DeviceInfo" + "Manufacturer: " + System.getProperty("os.name"));
        System.err.println("DeviceInfo" + "Model: " + System.getProperty("os.name"));
        System.err.println("DeviceInfo" + "Product: " + System.getProperty("os.name"));
        System.err.println("DeviceInfo" + getEndpointReference().toString());
    }

}
