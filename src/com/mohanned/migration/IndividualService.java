package com.mohanned.migration;

import org.ws4d.java.service.DefaultService;
import org.ws4d.java.types.QName;

import java.io.IOException;


public class IndividualService extends DefaultService {

    public IndividualService(String serviceType) throws IOException {
        super();
        QName QN_PORTTYPE = new QName(serviceType, Configuration.ws4dNamespace);
        this.addPortType(QN_PORTTYPE);
    }

   
    
    
}