package com.mohanned.migration;

import org.ws4d.java.service.Device;
import org.ws4d.java.service.Service;


public class MigClass {

    Service service;
    Device origin;
    Device destination;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Device getOrigin() {
        return origin;
    }

    public void setOrigin(Device origin) {
        this.origin = origin;
    }

    public Device getDestination() {
        return destination;
    }

    public void setDestination(Device destination) {
        this.destination = destination;
    }

    @Override
    public String toString() {
        return Data.getPureServiceName(getService()) + " / " +
                Data.getShortDeviceName(getOrigin()) + " -> " +
                Data.getShortDeviceName(getDestination());
    }
}
