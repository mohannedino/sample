package com.mohanned.migration;

import org.restlet.representation.Representation;
import org.restlet.resource.ClientResource;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.Service;

import java.io.IOException;

public class Migration {

    public String doMigration(Service service, Device source, Device destination)
            throws CommunicationException, IOException {

        String serName = Data.getShortServiceName(service).replaceAll(Configuration.ws4dNamespace, "");
        String sourceIP = Data.getDeviceIP(source);
        String destinationIP = Data.getDeviceIP(destination);


        /*
		 * packing
         */
        String souceUrl = "http://" + sourceIP + ":8080/restdroid/getWar/" + serName;
        ClientResource sourceClientResource = new ClientResource(souceUrl);
        Representation response = sourceClientResource.get();

        //System.err.println(response.getText());
        String warFilePath = response.getText();

        if (warFilePath.contains("false")) {
            System.err.println(warFilePath);
            return "Cancel";
        }

        //System.err.println(warFilePath);
        /*
		 * * sending
         */
        //String param2After = URLEncoder.encode(warFilePath, "UTF-8");
        String downloadURL = "http://" + destinationIP + ":8080/restdroid/download/" + sourceIP + "/port/8080/temp/" + warFilePath;
        ClientResource destinationClientResource = new ClientResource(downloadURL);
        response = destinationClientResource.get();
        //To see the result of Download WAR process.
        System.err.println(response.getText());

        //if(response.is)
        /**
         * ********** deploying ***********
         */
        //To get the real path of the WAR file on the destination, important to install it on Android
        //The path differs based on the destination's OS.
        String installUrl = "http://" + destinationIP + ":8080/restdroid/install/" + serName;
        destinationClientResource = new ClientResource(installUrl);
        response = destinationClientResource.get();
        String responseText = response.getText();
        System.err.println(responseText);

        //File warPathonDestinationProvider = new File("/webapps/" + serName + ".war");
        //File WebAppDir = new File("/sdcard/jetty/webapps/restdroid.war");
        //ClassJetty.install(warPathonDestinationProvider, serName);//, WebAppDir, "restdroid", true);
        if (responseText.contains("Successfully")) {
            System.out.println("Deploy Successful!");

            //Managing the JMEDS services
            //to add service to destination
            String NamespaceSerType = service.getPortTypes().next().toString();
            String SerType = NamespaceSerType.replace(Configuration.xtra, "");
            System.err.println(Restdroid.addService(serName, destinationIP, SerType));
            //to delete service from origin
            Restdroid.deleteService(sourceIP, serName);

        } else {
            System.out.println("Error: " + response.getText());
        }

        //remove service from reservation list in RestWar
        Restdroid.removeFromInMigrationProcess(sourceIP, serName);

        return "Done";
    }
}
