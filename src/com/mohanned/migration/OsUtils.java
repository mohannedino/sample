/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mohanned.migration;

/**
 *
 * @author mkz
 */
public final class OsUtils {

    private static String OS = null;

    public static String getOsName() {
        if (OS == null) {
            OS = System.getProperty("os.name");
        }
        return OS;
    }

    public static boolean isWindows() {
        return getOsName().startsWith("Windows");
    }

    public static boolean isAndroid() {
        return getOsName().startsWith("Linux");
    }

    public static String findJettyHome() {
        String home = null;
        if (isAndroid()) {
            home = "/sdcard/" + "/Jetty";
        } else if (isWindows()) {

            home = System.getProperty("jetty.home");
            if (home == null) {
                home = System.getenv("JETTY_HOME");
                if (home == null) {
                    System.err.println("No jetty home found ! please add -Djetty.home to your excution command.");
                    System.exit(1);
                    return null;
                }
            }
        }
        return home.replace('\\', '/');
    }

    public static String findJettyTempDir() {
        
        String temp = null;
        if (isAndroid()) {
            temp = "/webapps/tmp";
        } else if (isWindows()) {
            temp = "/webapps/temp";
        }
        
        temp = OsUtils.findJettyHome()+temp;
        return temp.replace('\\', '/');
    }
}
