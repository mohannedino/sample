package com.mohanned.migration;

import com.ws4d.droidcommander.ExploreActivity;
import java.text.DecimalFormat;

import java.util.ArrayList;
import java.util.List;

public class Proc {

    static List<double[][]> xPriorityMatrix = new ArrayList<double[][]>();
    static List<double[][]> x_PV = new ArrayList<double[][]>();
    public static String matrices = "\n\n";

    //Migs Array is an array of the first 10 founded migrations
    public static double[][] migs = new double[ExploreActivity.Migratedions.size()][ExploreActivity.critereaList.size()];
    ;
    /* ={
        /*mig1*///{70,	2,	   0.5},
		/*mig2*///{30,	1.5,     4},
		/*mig3*///{20,	2,	     2},
		/*mig4*///{90,  0.5,	 5}
	/*};*/


    //give a maximum value of infinite life time equal to 5 for the matrix consistency reason.

    //http://people.revoledu.com/kardi/tutorial/AHP/Comparison-Matrix.htm
    //Comparison Matrix is a diagonal matrix with diagonal elements equal to 1.
    //number of things is 'n'
    //number of comparison is n(n-1)/2
    //Comparison Matrix contains the actual judgment values of the right
    //and the reciprocal values of the left
    //fill the lower reciprocal matrix
    //a[j][i]=1/a[i][j]


    public static double[][] mainComparsion;

    //{
    /*Service Priority*/   //     {1.00,	0.33,	5.00},
    /*Origin Battery Life*/  //   {3.00,	1.00,	7.00},
    /*Destination Battery Life*///{0.2,	0.14,	1.00}
    //};
    //public static int n = ;
    //Generate a Paired comparison Matrix for Migrations based on each criteria.
    //static double servicePriorityMatrix[][]= new double[migs.length][migs.length];
    //static double originBatteryLifeMatrix[][]= new  double[migs.length][migs.length];
    //static double destinationBatteryLifeMatrix[][]= new  double[migs.length][migs.length];
    public static void main(String[] args) {
        int x = runAHP();
    }

    public static int runAHP() {
        //generate the priority vector
        double[][] main_PV = ahp(mainComparsion, ExploreActivity.critereaList.size(), "Main Comparsion Matrix");

        for (int z = 0; z < ExploreActivity.critereaList.size(); z++) {
            //Creating Migrations Comparison Matrix/Based on criterea

            //System.err.println();
            //System.err.println("Migration Comparsion Matrix based on Criterion : "+ TimerWork.critereaList.get(z).name.toString());
            //System.err.println();
            xPriorityMatrix.add(new double[ExploreActivity.Migratedions.size()][ExploreActivity.Migratedions.size()]);
            x_PV.add(ahp(initiateMatrix(xPriorityMatrix.get(z), z), ExploreActivity.Migratedions.size(),
                    ExploreActivity.critereaList.get(z).getRefinedName()));

            System.err.println();

        }

        //generate the priority vector for the migrations with respect to each criteria.
        //double[][] originBatteryLife_PV= ahp(initiateMatrix(originBatteryLifeMatrix, "originBatteryLife"),migs.length);
        //double[][] destinationBatteryLife_PV= ahp(initiateMatrix(destinationBatteryLifeMatrix, "destinationBatteryLife"),migs.length);
        double max = 0;
        int index = -1;

        //generate the overall weights for the migrations
        double[] result = new double[ExploreActivity.Migratedions.size()];
        for (int i = 0; i < ExploreActivity.Migratedions.size(); i++) {
            for (int j = 0; j < ExploreActivity.critereaList.size(); j++)// check length
            {
                result[i] += main_PV[j][0] * x_PV.get(j)[i][0];
                System.out.println("inner: " + result[i]);
            }

            //main_PV[1][0]*originBatteryLife_PV[i][0]+
            //main_PV[2][0]*destinationBatteryLife_PV[i][0];
            if (max < result[i]) {
                max = result[i];
                index = i;
            }
            System.out.println(result[i]);
        }
        if (result.length > 0) {
            System.err.println(result[index]);
        }

        return index;

    }

    private static double[][] ahp(double[][] x, int n, String MatrixName) {

        double[][] sum = new double[1][n];
        double[][] priorityVector = new double[n][1];

        for (int i = 0; i < n; i++) {
            sum[0][i] = 0;
        }

        for (int i = 0; i < n; i++) {
            priorityVector[i][0] = 0;
        }

        for (int j = 0; j < n; j++) {
            for (int i = 0; i < n; i++) {
                sum[0][j] += x[i][j];
            }
        }

        //normalized Eigen value
        double[][] normMatrix = new double[n][n];

        //calculate the priority Vector
        for (int i = 0; i < n; i++) {
            double tempSum = 0;
            for (int j = 0; j < n; j++) {
                normMatrix[i][j] = x[i][j] * Math.pow(sum[0][j], -1);
                tempSum += normMatrix[i][j];
            }
            priorityVector[i][0] = tempSum * Math.pow(n, -1);
        }
        //the sum of the elements of the Priority Vector must equal to 1.
        //shows relative weights among the compared things.
        //and get their ranking
        //the ration scale by dividing any two elements of the vector.. to see the priority between the compared things.

        //check consistency of the comparison matrix.
        //by calculating the Principal Eigen value which is the product result of sum vector and the Priority Vector.
        double LambdaMax = 0;
        for (int i = 0; i < n; i++) {
            LambdaMax += priorityVector[i][0] * Math.pow(normMatrix[i][i], -1);
        }

        //Calculate consistency index
        double CI = 0;
        CI = (LambdaMax - n) * Math.pow(n - 1, -1);
        //random index ,need to be passed to the algorithm later.
        double ri = 0;

        // ri is the random consistency index ;
        if (n <= 2) {
            ri = 0;
        } else if (n == 3) {
            ri = 0.58;
        } else if (n == 4) {
            ri = 0.90;
        } else if (n == 5) {
            ri = 1.12;
        } else if (n == 6) {
            ri = 1.24;
        } else if (n == 7) {
            ri = 1.32;
        } else if (n == 8) {
            ri = 1.41;
        } else if (n == 9) {
            ri = 1.45;
        } else if (n == 10) {
            ri = 1.49;
        } else if (n == 11) {
            ri = 1.51;
        } else if (n == 12) {
            ri = 1.48;
        } else if (n == 13) {
            ri = 1.56;
        } else if (n == 14) {
            ri = 1.57;
        } else if (n == 15) {
            ri = 1.59;
        }

        double CR = 0;
        CR = CI * Math.pow(ri, -1);
        System.err.println();
        //Consistency ratio CR is acceptable if it is <10%
        if (CR >= 0.1) {
            System.err.println("The matrix of '" + MatrixName + "' is inconsistent CR=" + get2decimals(CR) + "> 10%!");
            System.err.println();
            PrintMatrix(x, n, null);
            matrices += "The matrix of '" + MatrixName + "' is inconsistent CR=" + get2decimals(CR) + "> 10%!\n\n";
            return null;
        } else {
            System.err.println("The matrix of '" + MatrixName + "' is consistent CR=" + get2decimals(CR) + " LambdaMax = " + get2decimals(LambdaMax));
            System.err.println();
            PrintMatrix(x, n, priorityVector);
            matrices += "The matrix of '" + MatrixName + "' is consistent CR=" + get2decimals(CR) + " LambdaMax = " + get2decimals(LambdaMax) + "\n\n";
            return priorityVector;
        }

    }

    private static void PrintMatrix(double[][] x, int n, double[][] priorityVector) {
        //print the matrix

        for (int i = 0; i < x.length; i++) {
            for (int j = 0; j < x.length; j++) {
                System.err.print("\t" + get2decimals(x[i][j]));
                matrices += get2decimals(x[i][j]) + "\t";
            }
            System.err.println("     priorityVector[" + i + "] =" + get2decimals(priorityVector[i][0]));
            matrices += "     priorityVector[" + i + "] =" + get2decimals(priorityVector[i][0]) + "\n";
            System.err.println();
        }
    }

    static double[][] initiateMatrix(double[][] arr, int index) {
        for (int i = 0; i < arr.length; i++) {
            arr[i][i] = 1;
        }

        int colmnNumber = index;

        Criteria cr = ExploreActivity.critereaList.get(index);

        double range = cr.getNumericalValueWithHighestWeight() - cr.getNumericalValueWithLowestWeight();

        double fifthDiffernece = Math.abs(range) / 5;

        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                double x = migs[i][colmnNumber] - migs[j][colmnNumber];

                double absx = Math.abs(x);
                if (absx > 4 * fifthDiffernece) {
                    arr[i][j] = 9;
                } else if (absx <= 4 * fifthDiffernece && absx > 3 * fifthDiffernece) {
                    arr[i][j] = 7;
                } else if (absx <= 3 * fifthDiffernece && absx > 2 * fifthDiffernece) {
                    arr[i][j] = 5;
                } else if (absx <= 2 * fifthDiffernece && absx > fifthDiffernece) {
                    arr[i][j] = 3;
                } else if (absx <= fifthDiffernece) {
                    arr[i][j] = 1;
                }

                if (range * x < 0) {
                    arr[i][j] = Math.pow(arr[i][j], -1);
                } else if (range * x == 0) {
                    arr[i][j] = 1;
                }

                arr[j][i] = Math.pow(arr[i][j], -1);
            }
        }

        return arr;
    }

    static double[][] initiateMatrixold(double[][] arr, int index) {
        for (int i = 0; i < arr.length; i++) {
            arr[i][i] = 1;
        }

        int colmnNumber = index;

        Criteria cr = ExploreActivity.critereaList.get(index);

        double range = cr.getNumericalValueWithHighestWeight() - cr.getNumericalValueWithLowestWeight();

        double fifthDiffernece = Math.abs(range) / 5;

        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                double x = migs[i][colmnNumber] - migs[j][colmnNumber];
                double absx = Math.abs(x);
                if (x == 0) {
                    arr[i][j] = 1;
                } else if (range > 0) {
                    //make a comparison between destination Battery Lifetime values in order to map it into the AHP accepted judgment values : {1,3,5,7,9}

                    if (absx > 4 * fifthDiffernece) {
                        arr[i][j] = 9;
                    } else if (absx <= 4 * fifthDiffernece && absx > 3 * fifthDiffernece) {
                        arr[i][j] = 7;
                    } else if (absx <= 3 * fifthDiffernece && absx > 2 * fifthDiffernece) {
                        arr[i][j] = 5;
                    } else if (absx <= 2 * fifthDiffernece && absx > fifthDiffernece) {
                        arr[i][j] = 3;
                    } else if (absx <= fifthDiffernece) {
                        arr[i][j] = 1;
                    }

                    if (x < 0) {
                        arr[i][j] = Math.pow(arr[i][j], -1);
                    }

                } else if (range < 0) {
                    //make a comparison between destination Battery Lifetime values in order to map it into the AHP accepted judgment values : {1,3,5,7,9}

                    if (absx > 4 * fifthDiffernece) {
                        arr[i][j] = 1;
                    } else if (absx <= 4 * fifthDiffernece && absx > 3 * fifthDiffernece) //I made a change here 3
                    {
                        arr[i][j] = 3;
                    } else if (absx <= 3 * fifthDiffernece && absx > 2 * fifthDiffernece) {
                        arr[i][j] = 5;
                    } else if (absx <= 2 * fifthDiffernece && absx > fifthDiffernece) {
                        arr[i][j] = 7;
                    } else if (absx <= fifthDiffernece) {
                        arr[i][j] = 9;
                    }
                    //I made a change in >
                    if (x > 0) {
                        arr[i][j] = Math.pow(arr[i][j], -1);
                    }
                }
                arr[j][i] = Math.pow(arr[i][j], -1);
            }
        }

        return arr;
    }

    private static String get2decimals(double d) {
        String pattern = "###,###.##";
        DecimalFormat decimalFormat = new DecimalFormat(pattern);
        String format = decimalFormat.format(d);
        return format;
    }

}
