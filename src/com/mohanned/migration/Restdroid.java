/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mohanned.migration;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.restlet.data.Method;
import org.restlet.representation.Representation;
import org.restlet.resource.ClientResource;

/**
 *
 * @author Huho
 */
public class Restdroid {

    public Restdroid() {

    }

    public static String getServiceContext(String devIP, String devPort, String serviceName, String format) {
        String serviceContext = "";
        try {
            //changed to serviceName.toLowercase() as it causes error with Service with Capital letter.
            String serviceContextAPI = "http://" + devIP + ":" + devPort + "/" + serviceName.toLowerCase() + "/getServiceContext/" + format;
            ClientResource serviceContextRef = new ClientResource(serviceContextAPI);
            Representation response = serviceContextRef.get();
            serviceContext = response.getText();
        } catch (IOException ex) {
            Logger.getLogger(Restdroid.class.getName()).log(Level.SEVERE, null, ex);
        }
        return serviceContext;
    }

    public static String getProviderContext(String devName, String devIP, String devPort, String format) {
        String deviceContext = "";
        try {
            String deviceContextServiceAPI = "http://" + devIP + ":" + devPort + "/restdroid/getProviderContext/" + format + "/" + devName;
            ClientResource deviceRef = new ClientResource(deviceContextServiceAPI);
            System.err.println(deviceContextServiceAPI);
            Representation response = deviceRef.get();
            deviceContext = response.getText();
        } catch (IOException ex) {
            Logger.getLogger(Restdroid.class.getName()).log(Level.SEVERE, null, ex);
        }
        return deviceContext;
    }

    public static String isDeviceRunning(String devIP, String devPort) {
        String context = "";
        try {
            String api = "http://" + devIP + ":" + devPort + "/restdroid/device/get/isRunning";
            ClientResource ref = new ClientResource(api);
            Representation response = ref.get();
            context = response.getText();
        } catch (IOException ex) {
            Logger.getLogger(Restdroid.class.getName()).log(Level.SEVERE, null, ex);
        }
        return context;
    }

    public static String start(String devName, String devIP, String devPort) {
        String context = "";
        String api = "http://" + devIP + ":" + devPort + "/restdroid/device/post/start/"+devName;
        ClientResource start = new ClientResource(Method.POST, api);
        //Object myObject = devName;
        Representation response = start.post("");
        context = response.toString();
        return context;
    }

    public static String stop(String devName, String devIP, String devPort) {
        String context = "";
        /*try {
            String api = "http://" + devIP + ":" + devPort + "/device/delete/stop";
            ClientResource ref = new ClientResource(api);
            Representation response = ref.get();
            context = response.getText();
        } catch (IOException ex) {
            Logger.getLogger(Restdroid.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        
        return context;
    }

    public static String addService(String serviceName, String devIP, String serviceType) {
        String api = "http://" + devIP + ":8080/restdroid/service/post/" + serviceName+"/"+serviceType;        
        ClientResource add = new ClientResource(Method.POST, api);
        String status =  add.post("").toString();
        return "adding Service" + status;     
    }


   public static void deleteService(String devIP, String serviceName) {
       String api = "http://" + devIP + ":8080/restdroid/service/delete/" + serviceName;
        ClientResource ref = new ClientResource(api);
        System.err.println(ref.delete());
    }

   public static void removeFromInMigrationProcess(String devIP, String serviceName) {
       String api = "http://" + devIP + ":8080/restdroid/service/put/" + serviceName;
        ClientResource ref = new ClientResource(api);
        System.err.println(ref.delete());
    }
}
