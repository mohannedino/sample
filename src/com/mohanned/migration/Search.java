/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mohanned.migration;

import com.ws4d.droidcommander.SearchClient;
import org.ws4d.java.client.SearchManager;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.SearchParameter;

/**
 *
 * @author Huho
 */
public class Search {

    public static void searchAll() throws InterruptedException {
        SearchClient searchClient = new SearchClient();
        SearchParameter search1 = new SearchParameter();
        search1.setDeviceTypes(new QNameSet(new QName("Provider", Configuration.ws4dNamespace)));
        SearchManager.searchDevice(null, searchClient, null);

        System.err.println("Searching!!!");

    }

}
