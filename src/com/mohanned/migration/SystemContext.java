package com.mohanned.migration;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.ws4d.droidcommander.ExploreActivity;
import com.ws4d.droidcommander.SearchClient;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.security.SecurityKey;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.structures.Iterator;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Huho on 4/1/2016.
 */
public class SystemContext {

    static OntClass ontClass;

    public static OntModel generateModel() {

        Model data = ModelFactory.createDefaultModel();
        String RDFXML = Configuration.intialModel;

        try {
            data.read(new ByteArrayInputStream(RDFXML.getBytes("UTF-8")), null,
                    "RDF/XML");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        OntModel ontModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
        ontModel.add(data);
        return ontModel;

    }

    public static void InitializeModel() throws IOException {

        Configuration.initialRules.clear();
        Configuration.addSystemRules();

        ExploreActivity.Migratedions.clear();
        ExploreActivity.critereaList.clear();
        ExploreActivity.model = generateModel();
        System.err.println("timer" + "Model Generated :" + ExploreActivity.model.toString());
    }

    //create device instance in the model , device type is "ServiceProvider"
    public static void addDeviceToModel(Device provider) throws FileNotFoundException {
        Resource type = ExploreActivity.model.getResource(ExploreActivity.Namespace + "ServiceProvider");
        String providerName = Data.getShortDeviceName(provider);
        Individual instanceServiceProvider = ExploreActivity.model.createIndividual(ExploreActivity.Namespace + providerName, type);

        System.err.println("doMigration" + instanceServiceProvider.getLocalName() + "  is added to jena model");
        //Resource type = ExploreActivity.model.getResource(ExploreActivity.Namespace + "ServiceProvider");
//        OntClass ontClass = ExploreActivity.model.getOntClass(ExploreActivity.Namespace + "ServiceProvider");
//        Individual individual = ExploreActivity.model.createIndividual(ExploreActivity.Namespace + "HIIII", ontClass);
    }

    public static void addServiceToModel(ServiceReference service, DeviceReference device) throws FileNotFoundException, CommunicationException {
        //return;
        //Get Service Name
        String serviceName = Data.getShortServiceName(service.getService()).replace(Configuration.ws4dNamespace, "");

        //Get Service Type
        String NamespaceSerType = service.getPortTypes().next().toString();
        String SerType = NamespaceSerType.replace(Configuration.xtra, "");
        if (SerType.contains("deviceRef")) {
            SerType = SerType.replaceAll("deviceRef", "FrameworkService");
        }
        Resource serviceType = ExploreActivity.model.getResource(Configuration.Namespace + SerType);

        Individual instanceService = ExploreActivity.model.createIndividual(Configuration.Namespace + serviceName, serviceType);

        //Add Service to ServiceProvider
        System.err.println("doMigration" + serviceName.toString());

        Resource Provider = ExploreActivity.model.getResource(ExploreActivity.Namespace + Data.getShortDeviceName(device.getDevice()));
        Property provides = ExploreActivity.model.getProperty(ExploreActivity.Namespace + "provides");
        //Resource Service = ExploreActivity.model.getIndividual(ExploreActivity.Namespace + serviceName);
        Statement s = ResourceFactory.createStatement(Provider, provides, instanceService);

        ExploreActivity.model.add(s);
        System.err.println("doMigration" + s.getSubject() + "  " + s.getPredicate() + " " + s.getResource());

    }

    public static boolean isJsonString(String model) {
        try {
            JSONObject testModel = new JSONObject(model);
            //String result = JSONParser.quote(testModel);
        } catch (JSONException ex) {
            Logger.getLogger(SystemContext.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public static void addDeviceServicesContextIterator(DeviceReference device) throws CommunicationException, ParserConfigurationException, SAXException {

        String devIP = Data.getDeviceIP(device.getDevice());
        
        for (Iterator iterService = device.getDevice().getServiceReferences(SecurityKey.EMPTY_KEY); iterService.hasNext();) {
            ServiceReference sr = (ServiceReference) iterService.next();
            String serviceType = sr.getService().getPortTypes().next().toString();

            if (serviceType.contains("MigratableService")) {

                String serName = Data.getShortServiceName(sr.getService());
                String serviceName = serName.replaceAll(Configuration.ws4dNamespace, "");
                String serviceContext = Restdroid.getServiceContext(devIP, "8080", serviceName, "json");
                if (isJsonString(serviceContext)) {
                    try {
                        //add function of the json
                        JSONObject obj = new JSONObject(serviceContext);
                        addJSONResourceContextToModel(serviceName, obj);
                    } catch (JSONException ex) {
                        Logger.getLogger(SystemContext.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    addResourceContextToModel(serviceName, serviceContext);
                }

            }
        }
    }

    public static void addContext() {
        //if removed the condition a concurrent error arise!
        if (SearchClient.deviceList.isEmpty()) {
            return;
        }
        int x = SearchClient.deviceList.size();
        for (int i = 0; i < x; i++) {
            DeviceReference provider = SearchClient.deviceList.get(i);

            try {
                String devName = Data.getShortDeviceName(provider.getDevice());
                String devIP = Data.getDeviceIP(provider.getDevice());
                String deviceContext = Restdroid.getProviderContext(devName, devIP, "8080", "json");

                if (isJsonString(deviceContext)) {
                    //add function of the json
                    JSONObject obj = new JSONObject(deviceContext);
                    addJSONResourceContextToModel(devName, obj);

                } else {
                    addResourceContextToModel(devName, deviceContext);
                }

                addDeviceServicesContextIterator(provider);

            } catch (CommunicationException | ParserConfigurationException | SAXException e) {
                if (e.getMessage() == null) {
                    try {
                        System.err.println("doMigration" + e.toString() + " Unknown error " + Data.getShortDeviceName(provider.getDevice()) + "Android OS thread main error! ?");
                    } catch (CommunicationException ex) {
                        Logger.getLogger(SystemContext.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else if (e.getMessage().equals("Unexpected end of document")) {
                    try {
                        System.err.println("doMigration" + e.getMessage() + "    " + Data.getShortDeviceName(provider.getDevice()) + "might be Server not running");
                    } catch (CommunicationException ex) {
                        Logger.getLogger(SystemContext.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } catch (JSONException ex) {
                Logger.getLogger(SystemContext.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void addResourceContextToModel(String ResourceName, String XMLContext) {

        try {

            if (!XMLContext.contains("<?xml version=\"1.0\" encoding=\"utf-8\"?>")) {
                XMLContext = XMLContext.replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>", "");
            }

            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(XMLContext));
            DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = null;

            try {
                doc = dBuilder.parse(is);
            } catch (SAXException | IOException ex) {
                Logger.getLogger(SystemContext.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (doc == null) {
                throw new RuntimeException("Mohanned Msg: " + ResourceName + " model is not a valid XML file!");
            }
            addnoPreferences(ResourceName, doc);
            addHasProperties(ResourceName, doc);
            addRules(doc);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(SystemContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void addJSONResourceContextToModel(String ResourceName, JSONObject obj) {
        addJSONnoPreferences(ResourceName, obj);
        addHasJSONProperties(ResourceName, obj);
        addJSONRules(obj);
    }

    private static void addnoPreferences(String ResourceName, Document doc) {
        String noPreferenceRulesValue = doc.getElementsByTagName("noPreferenceRules").item(0).getTextContent();
        addNoPreferenceRules(ResourceName, noPreferenceRulesValue);
    }

    private static void addNoPreferenceRules(String ResouceName, String noPreferenceRulesValue) {
        Property noPreferenceRules = ExploreActivity.model.getProperty(ExploreActivity.Namespace + "noPreferenceRules");
        Literal Myboolean = null;
        if (noPreferenceRulesValue.contains("false")) {
            Myboolean = ExploreActivity.model.createTypedLiteral(false);
        } else {
            Myboolean = ExploreActivity.model.createTypedLiteral(true);
        }
        ExploreActivity.model.add(ExploreActivity.model.getResource(ExploreActivity.Namespace + ResouceName), noPreferenceRules, Myboolean);

        System.err.println("Statment added :\n (NoPreferenceRules," + ResouceName + "):\n[" + ExploreActivity.model.getResource(ExploreActivity.Namespace + ResouceName).toString() + " , " + noPreferenceRules.toString() + " , " + Myboolean.toString() + "]");
    }

    private static void addJSONnoPreferences(String ResourceName, JSONObject obj) {

        try {
            String noPreferenceRulesString = obj.getString("noPreferenceRules");
            boolean NoPreferenceRules = false;//Boolean.getBoolean(noPreferenceRulesString);

            if (noPreferenceRulesString.equals("false")) {
                NoPreferenceRules = false;
            } else if (noPreferenceRulesString.equals("true")) {
                NoPreferenceRules = true;
            }

            Property noPreferenceRules = ExploreActivity.model.getProperty(ExploreActivity.Namespace + "noPreferenceRules");
            Literal Myboolean = ExploreActivity.model.createTypedLiteral(NoPreferenceRules);
            ExploreActivity.model.add(ExploreActivity.model.getResource(ExploreActivity.Namespace + ResourceName), noPreferenceRules, Myboolean);
            System.err.println("Statment added :\n (NoPreferenceRules," + ResourceName + "):\n[" + ExploreActivity.model.getResource(ExploreActivity.Namespace + ResourceName).toString() + " , " + noPreferenceRules.toString() + " , " + Myboolean.toString() + "]");

        } catch (JSONException ex) {
            Logger.getLogger(SystemContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void addHasProperties(String ResourceName, Document doc) {
        NodeList propertiesList = doc.getElementsByTagName("hasProperty");
        for (int i = 0; i < propertiesList.getLength(); i++) {
            //list of properties
            Node propertyNode = propertiesList.item(i).getChildNodes().item(0).getNextSibling();
            String propName = null, propType = null, propValue = null;
            //add name of the property
            propName = propertyNode.getNodeName();
            List<String> tempCriteriaList = new ArrayList<String>();
            {
                //get childern of the proeprty : <propertyName><propertyValue><criteria><critria>
                NodeList propertyNodeChilds = propertyNode.getChildNodes();
                for (int t = 0; t < propertyNodeChilds.getLength(); t++) {
                    Node child = propertyNodeChilds.item(t);
                    if (child == null) {
                        return;
                    }
                    if (child.getNodeName().equals("propertyValue")) {
                        propValue = child.getTextContent();
                    } else if (child.getNodeName().equals("propertyType")) {
                        propType = child.getTextContent();
                    } else if (child.getNodeName().equals("criteria")) {
                        tempCriteriaList.add(child.getTextContent());

                    }
                }
            }

            addPropertyToModel(ResourceName, propName, propType, propValue, tempCriteriaList);
            tempCriteriaList.clear();
        }
    }

    private static void addHasJSONProperties(String ResourceName, JSONObject obj) {

        try {
            Object properties = obj.get("properties");
            if (properties instanceof JSONArray) {
                JSONArray propertyNodes = (JSONArray) properties;
                for (int i = 0; i < propertyNodes.length(); i++) {
                    JSONObject propertyNode = propertyNodes.getJSONObject(i);
                    addJSONProp(propertyNode, ResourceName);
                }
            } else if (properties instanceof JSONObject) {
                addJSONProp((JSONObject) properties, ResourceName);
            }

        } catch (JSONException ex) {
            Logger.getLogger(SystemContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void addJSONProp(JSONObject propertyNode, String ResourceName) throws JSONException {
        String propType, propValue, propName;
        //add type of the property
        propName = propertyNode.getString("propertyName");
        propType = propertyNode.getString("propertyType");
        propValue = propertyNode.getString("propertyValue");

        String s = propValue.getClass().getName();

        List<String> tempCriteriaList = new ArrayList<String>();
        java.util.Iterator keys = propertyNode.keys();
        boolean hasCriteria = false;
        while (keys.hasNext()) {
            if (keys.next().toString().equals("criteria")) {
                hasCriteria = true;
                break;
            }
        }

        if (hasCriteria) {
            Object criteria = propertyNode.get("criteria");
            if (criteria instanceof JSONArray) {
                JSONArray criteriaNodes = (JSONArray) criteria;
                for (int i = 0; i < criteriaNodes.length(); i++) {
                    String criterionNode = criteriaNodes.get(i).toString();
                    tempCriteriaList.add(criterionNode);
                }
            } else if (criteria instanceof String) {
                String criterionNode = criteria.toString();
                tempCriteriaList.add(criterionNode);
            }
        }
        addPropertyToModel(ResourceName, propName, propType, propValue, tempCriteriaList);
        tempCriteriaList.clear();
    }

    private static void addRules(Document doc) {
        for (int i = 0; i < doc.getElementsByTagName("rule").getLength(); i++) {
            String rule = doc.getElementsByTagName("rule").item(i).getTextContent();
            addRulesToModel(rule.trim());
        }
    }

    private static void addJSONRules(JSONObject obj) {
        java.util.Iterator keys = obj.keys();
        boolean hasRules = false;
        while (keys.hasNext()) {
            if (keys.next().toString().equals("rules")) {
                hasRules = true;
                break;
            }
        }
        if (hasRules) {
            try {
                Object rules = obj.get("rules");
                if (rules instanceof JSONArray) {
                    JSONArray rulesArray = (JSONArray) rules;
                    for (int i = 0; i < rulesArray.length(); i++) {
                        try {
                            String aRule = "";
                            aRule = rulesArray.get(i).toString();
                            //aRule.replace("\\\"", "\"");
                            addRulesToModel(aRule.trim());
                        } catch (JSONException ex) {
                            Logger.getLogger(SystemContext.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    }
                } else if (rules instanceof String) {
                    String aRule = "";
                    aRule = rules.toString();
                    //aRule.replace("\"", "\"");
                    addRulesToModel(aRule.trim());

                }
                //I added this as I had to modify the RULE in JSON by adding \" before any value described in the rule
            } catch (JSONException ex) {
                Logger.getLogger(SystemContext.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    private static void addPropertyToModel(String ResourceName, String propName, String propType, String propValue, List<String> tempCriteriaList) {
        if (propName != "" && propType != "" && propValue != "") {
            Resource PropertyName = ExploreActivity.model.getResource(ExploreActivity.Namespace + propName);

            Random rand = new Random();

            Individual InstancePropertyName = ExploreActivity.model.createIndividual(ExploreActivity.Namespace + PropertyName.getLocalName() + rand.nextInt(), PropertyName);

            System.err.println("PropertyInstance :\n (add PropertyName to the model for Resource" + ResourceName + "): " + propName + " called '" + InstancePropertyName.toString() + "'");

            Resource subject = ExploreActivity.model.getResource(ExploreActivity.Namespace + ResourceName);
            Property predicate = ExploreActivity.model.getProperty(ExploreActivity.Namespace + "hasProperty");
            Resource object = InstancePropertyName;

            Statement s = ResourceFactory.createStatement(subject, predicate, object);
            ExploreActivity.model.add(s);

            System.err.println("'" + InstancePropertyName.toString() + "' PropertyInstance \n attached to the Resource '" + ResourceName + "'):\n" + propName + "called " + InstancePropertyName.toString());

            Literal myType = ExploreActivity.model.createTypedLiteral(propType);
            //should be typed literal
            //Literal myValue = model.createTypedLiteral(new Integer(propValue) );
            int myIntPropValue = Integer.parseInt(propValue);
            Literal myValue = ExploreActivity.model.createTypedLiteral(myIntPropValue);

            Property propertyType = ExploreActivity.model.getProperty(ExploreActivity.Namespace + "propertyType");
            Property propertyValue = ExploreActivity.model.getProperty(ExploreActivity.Namespace + "propertyValue");
            Property criteria = ExploreActivity.model.getProperty(ExploreActivity.Namespace + "criteria");

            ExploreActivity.model.add(InstancePropertyName, propertyType, myType);
            ExploreActivity.model.add(InstancePropertyName, propertyValue, myValue);
            System.err.println("Statment added an:\n" + InstancePropertyName.toString() + " , " + propertyType.toString() + " , " + myType.toString());
            System.err.println("Statment added an:\n" + InstancePropertyName.toString() + " , " + propertyValue.toString() + " , " + myValue.toString());

            for (int i = 0; i < tempCriteriaList.size(); i++) {
                //if()tempCriteriaList.get(i).contains(ExploreActivity.Namespace))
                Resource criteriaValue = ExploreActivity.model.getResource(ExploreActivity.Namespace + tempCriteriaList.get(i));
                ExploreActivity.model.add(InstancePropertyName, criteria, criteriaValue);
                System.err.println("Statment added an:\n" + InstancePropertyName.toString() + " , " + criteria.toString() + " , " + criteriaValue.toString());

            }
            tempCriteriaList.clear();

        }
    }

    private static void addRulesToModel(String line) {
        String rule = line.replace("{Rule}", "");
        //check about rule double quotation 
        //ExploreActivity.rules += "\n" + rule;
        String myFinalRule = rule.replace("\\\"", "\"");

        Configuration.addRules(myFinalRule);

        System.err.println("Rule added: \n" + myFinalRule);
    }

    public static void printme(int i) {
        System.err.println("HI" + String.valueOf(i));
    }

    @Deprecated
    private static void addHasPropertiesDepricated(String ResourceName, Document doc) {
        NodeList properties = doc.getElementsByTagName("hasProperty");
        for (int i = 0; i < properties.getLength(); i++) {
            Node propertyNode = properties.item(i).getChildNodes().item(0).getNextSibling();
            String propName, propType = null, propValue = null;
            //add name of the property
            propName = propertyNode.getNodeName();

            List<String> tempCriteriaList = new ArrayList<String>();

            //get childern of the proeprty : <propertyName><propertyValue><criteria><critria>
            NodeList propertyNodeChilds = propertyNode.getChildNodes();
            for (int w = 0; w < propertyNodeChilds.getLength(); i++) {
                Node child = propertyNodeChilds.item(w).getNextSibling();
                if (child.getNodeName().equals("propertyValue")) {
                    propValue = child.getTextContent();
                } else if (child.getNodeName().equals("propertyType")) {
                    propType = child.getTextContent();
                } else if (child.getNodeName().equals("criteria")) {
                    tempCriteriaList.add(child.getTextContent());
                }
            }

            addPropertyToModel(ResourceName, propName, propType, propValue, tempCriteriaList);
            tempCriteriaList.clear();
        }
    }

    @Deprecated
    private static void addHasProperties2(String ResourceName, Document doc
    ) {
        //this still need to revised by the Android version of this procedure...
        NodeList properties = doc.getElementsByTagName("hasProperty");

        for (int i = 0; i < properties.getLength(); i++) {
            {
                String propName, propType = null, propValue = null;

                Node NodeI = properties.item(i);
                System.err.println("NodeI =" + NodeI.getNodeName());

                for (int j = 0; j < NodeI.getChildNodes().getLength(); j++) {

                    Node NodeJ = NodeI.getChildNodes().item(j);

                    if (!"#text".equals(NodeJ.getNodeName())) {
                        System.err.println("NodeJ = " + NodeJ.getNodeName());
                        propName = NodeJ.getNodeName();
                        List<String> tempCriteriaList = new ArrayList<String>();

                        Element eElement = (Element) NodeJ.getChildNodes();
                        Element propertyValue = (Element) eElement.getElementsByTagName("propertyValue").item(0);
                        Element propertyType = (Element) eElement.getElementsByTagName("propertyType").item(0);

                        System.out.println("propertyValue: " + propertyValue.getTextContent());
                        System.out.println("propertyType: " + propertyType.getTextContent());

                        propType = propertyType.getTextContent();
                        propValue = propertyValue.getTextContent();

                        NodeList criteria = eElement.getElementsByTagName("criteria");
                        for (int k = 0; k < criteria.getLength(); k++) {
                            if (!"#text".equals(criteria.item(k).getTextContent())) {
                                System.err.println("criteria = " + criteria.item(k).getTextContent());
                                tempCriteriaList.add(criteria.item(k).getTextContent());

                            }
                        }
                        //add name of the property
                        if (propName != null && propType != null && propValue != null) {
                            addPropertyToModel(ResourceName, propName, propType, propValue, tempCriteriaList);
                        }
                        tempCriteriaList.clear();

                    }
                }
            }
        }
    }

}
