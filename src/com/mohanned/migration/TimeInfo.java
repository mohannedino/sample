package com.mohanned.migration;

public class TimeInfo {
public enum status {
    StartAddingDevicesandServicestoModel,
    EndAddingDevicesandServicestoModel,

    StartSearchforMigrationintheModel,
    EndSearchforMigrationintheModel,

    StartMigrationDecisionMakingProcess,
    EndMigrationDecisionMakingProcess,

    StartPhysicalMigrationProcess,
    EndPhysicalMigrationProcess

}

    private String title;
    private long time;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getTime() {
        return time;
    }

    /*public void setTime(long time) {
        this.time = time;
    }*/


    public TimeInfo(status Title) {
        title = Title.name();
        time = System.currentTimeMillis();
    }
}
