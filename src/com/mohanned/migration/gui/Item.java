/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mohanned.migration.gui;

import com.mohanned.migration.Data;
import com.mohanned.migration.Restdroid;
import java.io.IOException;
import javax.swing.tree.DefaultMutableTreeNode;
import org.restlet.representation.Representation;
import org.restlet.resource.ClientResource;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.security.SecurityKey;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.ProxyDevice;
import org.ws4d.java.service.ProxyService;
import org.ws4d.java.service.Service;

/**
 *
 * @author Huho
 */
public class Item extends DefaultMutableTreeNode {

    public Object getUserObject() {
        if (this.getLevel() >= 0) {
            return userObject;
        } else {
            return null;
        }
    }

    public Item(Object userObject) {
        super(userObject);
    }

    @Override
    public String toString() {
        if (getUserObject() == null) {
            return "";
        }
        Object obj = getUserObject();
        if (ProxyDevice.class.isInstance(obj) || Device.class.isInstance(obj)) {
            return Data.getShortDeviceName((Device) obj) + " - " + Data.getDeviceIP((Device) obj);
        } else if (Service.class.isInstance(obj) || ProxyService.class.isInstance(obj)) {
            return Data.getPureServiceName((Service) obj);
        } else {
            return super.toString();
        }
    }

    @Deprecated
    public String getModelDeprecated() throws IOException, CommunicationException {
        String Model = "....";
        Object obj = getUserObject();
        if (ProxyDevice.class.isInstance(obj) || Device.class.isInstance(obj)) {
            String IP = Data.getDeviceIP((Device) obj);
            String devName = Data.getShortDeviceName((Device) obj);
            String deviceContextServiceAPI = "http://" + IP + ":8080/restdroid/getProviderContext/" + devName;
            ClientResource deviceRef = new ClientResource(deviceContextServiceAPI);
            Representation response = deviceRef.get();
            Model = response.getText();

        } else if (Service.class.isInstance(obj) || ProxyService.class.isInstance(obj)) {
            String devIP = Data.getDeviceIP(Service.class.cast(obj).getParentDeviceReference(SecurityKey.EMPTY_KEY).getDevice());
            String serviceName = Data.getPureServiceName((Service) obj);
            String serviceContextAPI = "http://" + devIP + ":8080/" + serviceName + "/getServiceContext";
            ClientResource serviceContextRef = new ClientResource(serviceContextAPI);
            Representation response = serviceContextRef.get();
            Model = response.getText();

        }
        return Model;
    }

    public String getModel() {

        String Model = "....";
        Object obj = getUserObject();
        if (ProxyDevice.class.isInstance(obj) || Device.class.isInstance(obj)) {
            String devIP = Data.getDeviceIP((Device) obj);
            String devName = Data.getShortDeviceName((Device) obj);

            Model = Restdroid.getProviderContext(devName, devIP, "8080", "json");

        } else if (Service.class.isInstance(obj) || ProxyService.class.isInstance(obj)) {
            try {
                String devIP = Data.getDeviceIP(Service.class.cast(obj).getParentDeviceReference(SecurityKey.EMPTY_KEY).getDevice());
                String serviceName = Data.getPureServiceName((Service) obj);
                Model = Restdroid.getServiceContext(devIP, "8080", serviceName, "json");
            } catch (CommunicationException ex) {
                return "";
            } catch (NullPointerException ex) {
                return "";
            }
        }
        return Model;
    }

}
