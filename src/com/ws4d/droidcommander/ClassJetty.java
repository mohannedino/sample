/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ws4d.droidcommander;

import com.mohanned.migration.OsUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;

//import org.mortbay.ijetty.log.AndroidLog;
import org.eclipse.jetty.util.resource.JarResource;
import org.eclipse.jetty.util.resource.Resource;

/**
 *
 * @author mkz
 */
public class ClassJetty {

    private static final String TAG = "Jetty";
    
    public static final String __TMP_DIR = "tmp";
    public static final File __JETTY_DIR;
    static
    {
        __JETTY_DIR = new File(OsUtils.findJettyHome(),"jetty"); 
        // Ensure parsing is not validating - does not work with android
        System.setProperty("org.eclipse.jetty.xml.XmlParser.Validating","false");

        // Bridge Jetty logging to Android logging
        System.setProperty("org.eclipse.jetty.util.log.class","org.mortbay.ijetty.AndroidLog");
        //org.eclipse.jetty.util.log.Log.setLog(new AndroidLog());
    }
    public static final String __CONTEXTS_DIR = "contexts";
    public static final String __WEBAPP_DIR = "webapps";
    public static final String[] __configurationClasses
            = new String[]{
                "org.mortbay.ijetty.webapp.AndroidWebInfConfiguration",
                "org.eclipse.jetty.webapp.WebXmlConfiguration",
                "org.eclipse.jetty.webapp.JettyWebXmlConfiguration",
                "org.eclipse.jetty.webapp.TagLibConfiguration"
            };

    public static void install(File file, String path) {
        try {
            File webappDir = new File(__JETTY_DIR + "/" + __WEBAPP_DIR);
            String name = file.getName();
            if (name.endsWith(".war") || name.endsWith(".jar")) {
                name = name.substring(0, name.length() - 4);
            }

            install(file, path, webappDir, name, true);

        } catch (Exception e) {
            System.err.println("Jetty"+ "Bad resource"+ e);
        }

    }

    public static void install(File warFile, String contextPath, File webappsDir, String webappName, boolean createContextXml)
            throws MalformedURLException, IOException {
        File webapp = new File(webappsDir, webappName);
        if (!webapp.exists()) {
            webapp.mkdirs();
        }

        Resource war = Resource.newResource("jar:" + warFile.toURL() + "!/");
        ((JarResource) war).copyTo(webapp);
        if (createContextXml) {
            installContextFile(webappName, contextPath);
        }
    }

    public static void installContextFile(String webappName, String contextPath)
            throws FileNotFoundException {
        System.err.println(TAG+ "Installing " + webappName + ".xml");
        contextPath = contextPath == null ? webappName : contextPath;
        contextPath = contextPath.equals("/") ? "root" : contextPath;
        contextPath = contextPath.startsWith("/") ? contextPath : "/" + contextPath;

        String configurationClassesXml = "<Array type=\"java.lang.String\">";
        for (int i = 0; i < __configurationClasses.length; i++) {
            configurationClassesXml += "<Item>" + __configurationClasses[i] + "</Item>";
        }
        configurationClassesXml += "</Array>";

        File tmpDir = new File(__JETTY_DIR + "/" + __TMP_DIR);
        File tmpContextFile = new File(tmpDir, webappName + ".xml");

        PrintWriter writer = new PrintWriter(tmpContextFile);
        writer.println("<?xml version=\"1.0\"  encoding=\"ISO-8859-1\"?>");
        writer.println("<!DOCTYPE Configure PUBLIC \"-//Jetty//Configure//EN\" \"http://www.eclipse.org/jetty/configure.dtd\">"
        );
        writer.println("<Configure class=\"org.eclipse.jetty.webapp.WebAppContext\">");
        writer.println("<Set name=\"configurationClasses\">" + configurationClassesXml + "</Set>");
        writer.println("<Set name=\"contextPath\">" + contextPath + "</Set>");
        writer.println("<Set name=\"war\"><SystemProperty name=\"jetty.home\" default=\".\"/>/webapps/" + webappName + "</Set>");
        writer.println("<Set name=\"defaultsDescriptor\"><SystemProperty name=\"jetty.home\" default=\".\"/>/etc/webdefault.xml</Set>");

        writer.println("</Configure>");
        writer.flush();
        writer.close();
        File contextDir = new File(__JETTY_DIR + "/" + __CONTEXTS_DIR);
        File contextFile = new File(contextDir, webappName + ".xml");
        if (!tmpContextFile.renameTo(contextFile)) {
            System.err.println(TAG+ "mv " + tmpContextFile.getAbsolutePath() + " " + contextFile.getAbsolutePath() + " failed");
        }
    }
}
