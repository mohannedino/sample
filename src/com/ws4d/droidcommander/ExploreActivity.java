package com.ws4d.droidcommander;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.InfModel;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.reasoner.Reasoner;
import com.hp.hpl.jena.reasoner.rulesys.GenericRuleReasoner;
import com.hp.hpl.jena.reasoner.rulesys.Rule;
import com.mohanned.migration.Configuration;
import com.mohanned.migration.Criteria;
import com.mohanned.migration.Data;
import com.mohanned.migration.MigClass;
import com.mohanned.migration.Migration;
import com.mohanned.migration.OsUtils;
import com.mohanned.migration.Proc;
import com.mohanned.migration.Search;
import com.mohanned.migration.SystemContext;
import com.mohanned.migration.TimeInfo;
import com.mohanned.migration.gui.NewApplication;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.security.SecurityKey;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.URI;
import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;

import static java.lang.String.*;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ws4d.java.JMEDSFramework;
import org.ws4d.java.communication.DPWSProtocolVersion;
import org.ws4d.java.configuration.DPWSProperties;
import org.ws4d.java.constants.DPWS2006.DPWSConstants2006;

public class ExploreActivity {

    static final int MSG_DEVICE_FOUND = 1;

    // list to store the found devices
    // items added through deviceFound method in
    // discovery_manager.SearchClient.java
    // list to store the found services
    // items added through serviceFound method in
    // discovery_manager.SearchClient.java
    //public static ArrayList<DeviceReference> dlist = new ArrayList<DeviceReference>();
    private static ArrayList<Classy> classss = new ArrayList<Classy>();
    //public static ArrayList<ServiceReference,String> slist = new ArrayList<ServiceReference>();

    public static boolean busy = false;
    public static OntModel model = null;
    public static InfModel inf = null;
    //to save the property value of mig/criteria matrix
    public static String arrayFull = "";

    public static List<MigClass> Migratedions = new ArrayList<MigClass>();
    public static List<Criteria> critereaList = new ArrayList<Criteria>();
    public static ArrayList<TimeInfo> executionTime = new ArrayList<TimeInfo>();

    public static List<String> candidatedServices = new ArrayList<String>();
    public static List<String> candidatedOrigins = new ArrayList<String>();
    public static List<String> candidatedDestinations = new ArrayList<String>();

    //Model Namespace
    public static String Namespace = Configuration.Namespace;

    private static final String TAG = "DPWSExample";

    public static void main(String[] args) throws IOException, InterruptedException, CommunicationException, ParserConfigurationException, SAXException {
        if (!JMEDSFramework.isRunning()) {
            JMEDSFramework.start(args);
        }

        DPWSProperties.getInstance()
                .removeSupportedDPWSVersion(new DPWSProtocolVersion(DPWSConstants2006.DPWS_VERSION2006));

        Search.searchAll();

        while (SearchClient.deviceList.size() < 5);
        {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(NewApplication.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        initModel();
        getCriteriaList(model);

        //region Query for Migration
        printMe("|_Start checking model for violations...", null);
        QueryForMigration(inf);
        printMe("|_End checking model for violations...", null);
        //endregion Query for Migration

        int migIndex = migrationDecisionMakingProcess(inf, critereaList);
        doPhysicalMigration(migIndex);
        String debugging = processingTimePrint();
        saveModels(inf, debugging);
    }

    public static void tryRun() {
        Object app = NewApplication.thatGUIForm;
        try {
            Search.searchAll();
            while (SearchClient.deviceList.size() < 2);
            {
                System.err.println("I am Waiting " + SearchClient.deviceList.size());
                /*try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(NewApplication.class.getName()).log(Level.SEVERE, null, ex);
            }*/
            }

            initModel();
            getCriteriaList(model);

            double[][] mainComparsion = createMainComparsion();
            if (app != null) {
                ((NewApplication) app).setAHPCriteriaListAndMainComparsionMatrix(critereaList, mainComparsion);
            }

            //region Query for Migration
            printMe("|_Start checking model for violations...", null);
            QueryForMigration(inf);
            printMe("|_End checking model for violations...", null);
            //endregion Query for Migration

            if (app != null) {
                candidatedServices = QueryCandidated(inf, "?x rdf:type g:CandidateForMigrationService .\n");
                candidatedOrigins = QueryCandidated(inf, "?x rdf:type g:CandidateOriginServiceProvider .\n");
                candidatedDestinations = QueryCandidated(inf, "?x rdf:type g:CandidateDestinationServiceProvider .\n");
                ((NewApplication) app).setMigrationLists(Migratedions, candidatedServices, candidatedOrigins, candidatedDestinations);

            }

            int migIndex = migrationDecisionMakingProcess(inf, critereaList);

            //saveModels(inf, "");
            if (app != null) {
                ((NewApplication) app).setArrayOfMigsandCriteria(arrayFull);

            }

            String Status = doPhysicalMigration(migIndex);
            //cancel status comes from getWar when the service is not in migration process or does not exist on server.
            //value transfered by doMigration to doPhysicalMigration 
            if (Status.contains("Done")) {
                String debugging = processingTimePrint();
                saveModels(inf, debugging);
                System.err.println(debugging);

                if (app != null) {

                    String thisInfo = "";
                    if (migIndex >= 0) {
                        MigClass mig = Migratedions.get(migIndex);
                        thisInfo = "Service : " + Data.getPureServiceName(mig.getService()) + "is chosen to be migrated to :"
                                + Data.getShortDeviceName(mig.getDestination());
                    } else {
                        thisInfo = "There is no migration required!";
                    }

                    ((NewApplication) app).setFinalMigrationAndProcessTime(thisInfo, debugging, Proc.matrices);

                }

            }

            /*
            try {
            
            try {
            Search.searchAll(app);
            initModel();
            getCriteriaList(model);
            double[][] mainComparsion = createMainComparsion();
            if (app != null) {
            ((NewApplication) app).setAHPCriteriaListAndMainComparsionMatrix(critereaList, mainComparsion);
            }
            } catch (IOException | CommunicationException | InterruptedException | ParserConfigurationException | SAXException ex) {
            Logger.getLogger(ExploreActivity.class.getName()).log(Level.SEVERE, null, ex);
            }

            QueryForMigration(inf);
            if (app != null) {
            ((NewApplication) app).setMigrationLists(Migratedions);
            }
            
            } catch (CommunicationException ex) {
            Logger.getLogger(ExploreActivity.class.getName()).log(Level.SEVERE, null, ex);
            }
             */
        } catch (InterruptedException ex) {
            Logger.getLogger(ExploreActivity.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ExploreActivity.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CommunicationException ex) {
            Logger.getLogger(ExploreActivity.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(ExploreActivity.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(ExploreActivity.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void refreshDevices() throws InterruptedException {
        //clearListOfDevices();
        Search.searchAll();
    }

    public void repeat() throws InterruptedException {
        /*refreshDevices();
        if (dlist.size() >= 3) {

            try {
                busy = true;
                doMigration();
                //busy = false;
            } catch (IOException ex) {
                Logger.getLogger(ExploreActivity.class.getName()).log(Level.SEVERE, null, ex);
            } catch (CommunicationException ex) {
                Logger.getLogger(ExploreActivity.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                Logger.getLogger(ExploreActivity.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParserConfigurationException ex) {
                Logger.getLogger(ExploreActivity.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SAXException ex) {
                Logger.getLogger(ExploreActivity.class.getName()).log(Level.SEVERE, null, ex);
            }

        }*/
    }

    public void fn(DeviceReference devRef) throws CommunicationException {
        String deviceName = Data.getShortDeviceName(devRef.getDevice());
        boolean found = false;
        for (int i = 0; i < SearchClient.deviceList.size(); i++) {
            if (Data.getShortDeviceName(SearchClient.deviceList.get(i).getDevice()).startsWith(deviceName)) {
                found = true;
                break;
            }
        }

        if (!found) {
            SearchClient.deviceList.add(devRef);
            String myDevice = Data.getShortDeviceName(devRef.getDevice());
            printMe(myDevice + " is found, device array size =" + SearchClient.deviceList.size(), null);

            Iterator serviceIterator = devRef.getDevice().getServiceReferences(SecurityKey.EMPTY_KEY);
            while (serviceIterator.hasNext()) {
                Object myService = serviceIterator.next();
                boolean serviceFound = false;
                for (int j = 0; j < classss.size(); j++) {
                    if ((myDevice == classss.get(j).deviceName)
                            && ((ServiceReference) myService).getServiceId().toString().contains(classss.get(j).serviceReference.getServiceId().toString())) {
                        serviceFound = true;
                        break;
                    }
                }

                if (!serviceFound) {
                    Classy serviceClass = new Classy();
                    serviceClass.deviceName = myDevice;
                    serviceClass.serviceReference = ((ServiceReference) myService);
                    classss.add(serviceClass);
                }
            }
        }
    }

    public void clearListOfDevices() {
        SearchClient.deviceList.clear();
        // dlist.clear();
    }

    public static void initModel() throws IOException, CommunicationException, InterruptedException, ParserConfigurationException, SAXException {

        executionTime.add(new TimeInfo(TimeInfo.status.StartAddingDevicesandServicestoModel));
        //region AddingDevicesandServicestoModel
        printMe("Start building system Model.", null);

        //Create Model
        //printMe("|_>Start Initialize Model", null);
        SystemContext.InitializeModel();
        //printMe("|_>End Initialize Model", null);

        //Number of Found Devices
        int x = SearchClient.deviceList.size();

        //if (x >= 3) 
        {
            //region Create System Model Process
            //Save Time of StartAddingDevicesandServicestoModel
            for (int i = 0; i < x; i++) {
                DeviceReference deviceRef = SearchClient.deviceList.get(i);
                String stringDeviceName = Data.getShortDeviceName(deviceRef.getDevice());
                //A- add Device to Model
                printMe("|_>Start adding Device to model : " + stringDeviceName, null);
                SystemContext.addDeviceToModel(deviceRef.getDevice());
                Iterator serviceReferences = deviceRef.getDevice().getServiceReferences(SecurityKey.EMPTY_KEY);

                //B- add Device's services to Model
                while (serviceReferences.hasNext()) {
                    ServiceReference serviceNext = (ServiceReference) serviceReferences.next();
                    if (!serviceNext.getService().getServiceId().equals(null)) {
                        if (serviceNext.getService().getPortTypes().next().toString().contains("MigratableService")) {
                            printMe("|__>adding Service to model : " + Data.getPureServiceName(serviceNext.getService()), null);
                        }
                        SystemContext.addServiceToModel(serviceNext, deviceRef);
                    }
                }
                printMe("|_>End adding Device to model:" + stringDeviceName, null);
            }

            //C-Fetch partial models of devices and services -- add them to the model
            SystemContext.addContext();

            printMe("|_>End Adding Devices and Services to Model", null);
            //Save Time of StartAddingDevicesandServicestoModel

            //endregion Create System Model Process
            //region initiate rules array for the reasoner
            String rules = "";
            java.util.Iterator<String> FullRules = Configuration.initialRules.iterator();
            while (FullRules.hasNext()) {

                rules += FullRules.next().toString() + "\n";

            }
            //endregion initiate rules array for the reasoner

            //region model inferring process
            //printMe("|_Start Model Inferring", null);
            Reasoner reasoner = new GenericRuleReasoner(Rule.parseRules(rules));
            reasoner.setDerivationLogging(true);

            inf = ModelFactory.createInfModel(reasoner, (Model) model);
            inf.prepare();

            executionTime.add(new TimeInfo(TimeInfo.status.EndAddingDevicesandServicestoModel));
        }
    }

    private static String doPhysicalMigration(int migIndex) {
        executionTime.add(new TimeInfo(TimeInfo.status.StartPhysicalMigrationProcess));
        String Status = "";
        //region PhysicalMigrationProcess
        if (migIndex >= 0) {

            try {
                Migration migration = new Migration();
                MigClass migInfo = Migratedions.get(migIndex);

                printMe("Service : " + migInfo.getService().getServiceId().toString().replace(Namespace, "") + "\n "
                        + " is chosen to be migrated to :" + " \n"
                        + Data.getShortDeviceName(migInfo.getDestination()), null);

                Status = migration.doMigration(migInfo.getService(), migInfo.getOrigin(), migInfo.getDestination());

                System.err.println();
                System.err.println("Migration is finished!\n");

            } catch (CommunicationException | IOException e) {
                Status += " - Error in doMigration Procedure";
            }
            // TODO Auto-generated catch block

        } else {
            Status = "There is no migration required!";
            printMe("There is no migration required!", null);
        }

        //endregion PhysicalMigrationProcess
        executionTime.add(new TimeInfo(TimeInfo.status.EndPhysicalMigrationProcess));

        return Status;
    }

    private static String processingTimePrint() {
        TimeInfo StartAddingDevicesandServicestoModel = null;
        TimeInfo EndAddingDevicesandServicestoModel = null;
        TimeInfo StartSearchforMigrationintheModel = null;
        TimeInfo EndSearchforMigrationintheModel = null;
        TimeInfo StartMigrationDecisionMakingProcess = null;
        TimeInfo EndMigrationDecisionMakingProcess = null;
        TimeInfo StartPhysicalMigrationProcess = null;
        TimeInfo EndPhysicalMigrationProcess = null;

        for (int i = 0; i < executionTime.size(); i++) {
            if (executionTime.get(i).getTitle().equals(TimeInfo.status.StartAddingDevicesandServicestoModel.name())) {
                StartAddingDevicesandServicestoModel = executionTime.get(i);
            } else if (executionTime.get(i).getTitle().equals(TimeInfo.status.EndAddingDevicesandServicestoModel.name())) {
                EndAddingDevicesandServicestoModel = executionTime.get(i);
            } else if (executionTime.get(i).getTitle().equals(TimeInfo.status.StartSearchforMigrationintheModel.name())) {
                StartSearchforMigrationintheModel = executionTime.get(i);
            } else if (executionTime.get(i).getTitle().equals(TimeInfo.status.EndSearchforMigrationintheModel.name())) {
                EndSearchforMigrationintheModel = executionTime.get(i);
            } else if (executionTime.get(i).getTitle().equals(TimeInfo.status.StartMigrationDecisionMakingProcess.name())) {
                StartMigrationDecisionMakingProcess = executionTime.get(i);
            } else if (executionTime.get(i).getTitle().equals(TimeInfo.status.EndMigrationDecisionMakingProcess.name())) {
                EndMigrationDecisionMakingProcess = executionTime.get(i);
            } else if (executionTime.get(i).getTitle().equals(TimeInfo.status.StartPhysicalMigrationProcess.name())) {
                StartPhysicalMigrationProcess = executionTime.get(i);
            } else if (executionTime.get(i).getTitle().equals(TimeInfo.status.EndPhysicalMigrationProcess.name())) {
                EndPhysicalMigrationProcess = executionTime.get(i);
            }

        }

        String debugging = "Debug Info:"
                + "\nTime to build full System Context Model: "
                + valueOf((EndAddingDevicesandServicestoModel.getTime()
                        - StartAddingDevicesandServicestoModel.getTime()) * 0.001) + " sec";

        debugging += "\nTime to find possible migration: "
                + valueOf((EndSearchforMigrationintheModel.getTime() - StartSearchforMigrationintheModel.getTime()) * 0.001) + " sec";

        debugging += "\nTime to Make a decision by AHP: "
                + valueOf((EndMigrationDecisionMakingProcess.getTime() - StartMigrationDecisionMakingProcess.getTime()) * 0.001) + " sec";

        debugging += "\nTime to Physical migration of the service: "
                + valueOf((EndPhysicalMigrationProcess.getTime() - StartPhysicalMigrationProcess.getTime()) * 0.001) + " sec";

        debugging += "\nTotal time:"
                + valueOf((EndPhysicalMigrationProcess.getTime() - StartAddingDevicesandServicestoModel.getTime()) * 0.001) + " sec";
        return debugging;
    }

    private static void saveModels(InfModel inf, String debugging) {
        //region Display text on GUI and save files
        FileWriter out = null;
        try {
            long time = new Date().getTime();
            out = new FileWriter(OsUtils.findJettyTempDir() + "/Normal/INF-NormalApp-" + time + ".owl");
            inf.write(out, "N-TRIPLE");
            out = new FileWriter(OsUtils.findJettyTempDir() + "/Normal/MODEL-NormalApp-" + time + ".owl");
            model.writeAll(out, "RDF/XML-ABBREV", null);

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
                    out.flush();
                    out.close();
                } catch (IOException ignore) {
                }
            }
        }
        //endregion Display text on GUI and save files
    }

    private static void QueryForMigration(InfModel inf) throws CommunicationException {

        executionTime.add(new TimeInfo(TimeInfo.status.StartSearchforMigrationintheModel));
        //region SearchforMigrationintheModel
        String queryString = "PREFIX  g:    <" + Namespace + ">\n"
                + "PREFIX  rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
                + "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
                + "\n"
                + "SELECT distinct ?origin ?destination ?service\n"
                + "WHERE\n"
                + "  { "
                + "?service <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> g:CandidateForMigrationService .\n"
                + "?origin <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> g:CandidateOriginServiceProvider .\n"
                + "?destination <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> g:CandidateDestinationServiceProvider .\n"
                + "?origin g:provides ?service .\n"
                + "?service g:possibleDestinationProvider ?destination .\n"
                + "?destination g:possibleProvidedService ?service .\n"
                + //                "FILTER NOT EXISTS {" +
                //   "FILTER (!regex(?origin, ?destination)) ." +
                //               "}" +
                "  }\n"
                + "";

        Query query = QueryFactory.create(queryString);
        // Execute the query and obtain results
        QueryExecution qe = QueryExecutionFactory.create(query, inf);

        int i = 0;
        ResultSet results = qe.execSelect();
        while (results.hasNext()) {
            i++;
            QuerySolution row = results.nextSolution();

            RDFNode origin = row.get("origin");
            RDFNode service = row.get("service");
            RDFNode destination = row.get("destination");

            String[] info = {origin.toString(), service.toString(), destination.toString()};

            DeviceReference temporigin = getDeviceFromList(origin.toString().replace(Namespace, ""));
            DeviceReference tempdestination = getDeviceFromList(destination.toString().replace(Namespace, ""));

            ServiceReference tempservice = getServiceFromList(service.toString().replace(Namespace, ""), temporigin);

            MigClass mig = new MigClass();
            mig.setService(tempservice.getService());
            mig.setOrigin(temporigin.getDevice());
            mig.setDestination(tempdestination.getDevice());

            printMe("Possible Found Migration # " + i + " is " + info[0].replace(Configuration.Namespace, "") + " "
                    + info[1].replace(Configuration.Namespace, "") + " "
                    + info[2].replace(Configuration.Namespace, ""), null);

            if (!Migratedions.contains(mig)) {
                Migratedions.add(mig);
            }

        }
        qe.close();

        //endregion SearchforMigrationintheModel
        executionTime.add(new TimeInfo(TimeInfo.status.EndSearchforMigrationintheModel));

    }

    private static int migrationDecisionMakingProcess(InfModel inf, List<Criteria> critereaList) {
        executionTime.add(new TimeInfo(TimeInfo.status.StartMigrationDecisionMakingProcess));
        //region MigrationDecisionMakingProcess
        int migIndex = -1;
        if (Migratedions.size() == 0) {
            migIndex = -1;
        } else if (Migratedions.size() == 1) {
            migIndex = 0;
        } else if (Migratedions.size() > 1) {

            // critereaList is created by mainComparsion
            double[][] mainComparsion = createMainComparsion();
            Proc.mainComparsion = mainComparsion;

            printMe("*****Main Migration Matrix*****", null);

            String arrayColumns = "";

            for (int z = 0; z < critereaList.size(); z++) {
                arrayColumns += "\t" + critereaList.get(z).getRefinedName();
            }

            String arrayRows = "";
            for (int y = 0; y < Migratedions.size(); y++) {
                arrayRows += "\nMig" + y;
                for (int z = 0; z < critereaList.size(); z++) {
                    MigClass migrationIndex = Migratedions.get(y);
                    Criteria criteriaIndex = critereaList.get(z);
                    String value = getCriteriaValueOfMigration(inf, migrationIndex, criteriaIndex);
                    printMe(Migratedions.get(y).toString() + " has index : " + y + ", considered criteria :" + critereaList.get(z).getName()
                            + " modelValue: " + value, null);
                    Double x = Data.getDouble(value);
                    Proc.migs[y][z] = x;
                    printMe("\t" + Proc.migs[y][z], null);
                    arrayRows += "\t\t" + x;
                }
                printMe("", null);
            }

            arrayFull = arrayColumns + arrayRows;

            migIndex = Proc.runAHP();
        }
        //endregion MigrationDecisionMakingProcess
        executionTime.add(new TimeInfo(TimeInfo.status.EndMigrationDecisionMakingProcess));
        return migIndex;
    }

    private static String getCriteriaValueOfMigration(InfModel inf, MigClass migClass, Criteria criteria) {

        //System.err.println(criteria.getName().toString());
        String thing = null;
        if (criteria.getRefinedOwner().contains("service")) {
            thing = Data.getShortServiceName(migClass.getService());
        } else if (criteria.getRefinedOwner().contains("origin")) {
            thing = Data.getShortDeviceName(migClass.getOrigin());
        } else if (criteria.getRefinedOwner().contains("destination")) {
            thing = Data.getShortDeviceName(migClass.getDestination());
        }

        if (thing.contains(Configuration.ws4dNamespace)) {
            thing = thing.replaceAll(Configuration.ws4dNamespace, "");
        }

        String queryGetMatchedCriteriaString = "PREFIX  g:    <" + Namespace + ">\n"
                + "PREFIX  rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
                + "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
                + "\n"
                + "SELECT distinct ?thing ?propertyType ?propertyValue ?name  ?owner ?CriteriaPriority  \n"
                + "WHERE\n"
                + "  { "
                + "?Property g:criteria ?CriteriaProperty  .\n"
                + "?Property g:propertyType ?propertyType.\n"
                + "?Property g:propertyValue ?propertyValue.\n"
                + "?CriteriaProperty g:name ?name .\n"
                + "?CriteriaProperty g:owner ?owner .\n"
                + "?CriteriaProperty g:CriteriaPriority ?CriteriaPriority .\n"
                + "?thing g:hasProperty ?Property .\n"
                + " FILTER ("
                + "?name = " + criteria.getName().toString() + "  && "
                + "?owner = " + criteria.getOwner().toString() + "  && "
                + "?CriteriaPriority = " + criteria.getCriteriaPrtiority().toString() + "  && "
                + "?thing = g:" + thing.toString()
                + " ) .\n"
                + "  }\n"
                + "";
        Query query = QueryFactory.create(queryGetMatchedCriteriaString);
        // Execute the query and obtain results
        QueryExecution qe = QueryExecutionFactory.create(query, inf);
        // Output query results
        //ResultSetFormatter.out(System.err, qe.execSelect(), query);

        ResultSet results = qe.execSelect();
        RDFNode propertyValue = null;
        while (results.hasNext()) {
            //i++;
            QuerySolution row = results.nextSolution();
            propertyValue = row.get("propertyValue");
            //RDFNode propertyType=	row.get("propertyType");
        }

        if (propertyValue != null) {
            return propertyValue.toString();
        } else {
            return null;
        }
    }

    private static void getCriteriaList(InfModel localModel) {

        String queryAllCritereaString = "PREFIX  g:    <" + Namespace + ">\n"
                + "PREFIX  rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
                + "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
                + "\n"
                + "SELECT distinct ?name ?owner ?CriteriaPriority ?valueWithHighestWeight ?valueWithLowestWeight \n"
                + "WHERE\n"
                + "  { "
                // + "?Property g:criteria ?CriteriaProperty .\n"
                + "?CriteriaProperty g:name ?name .\n"
                + "?CriteriaProperty g:owner ?owner .\n"
                + "?CriteriaProperty g:CriteriaPriority ?CriteriaPriority .\n"
                + "?CriteriaProperty g:valueWithHighestWeight ?valueWithHighestWeight .\n"
                + "?CriteriaProperty g:valueWithLowestWeight ?valueWithLowestWeight .\n"
                + "  }\n"
                + "";
        Query query = QueryFactory.create(queryAllCritereaString);
        // Execute the query and obtain results
        QueryExecution qe = QueryExecutionFactory.create(query, localModel);
        ResultSet results = qe.execSelect();
        while (results.hasNext()) {
            //i++;
            QuerySolution row = results.nextSolution();

            RDFNode name = row.get("name");
            RDFNode owner = row.get("owner");
            RDFNode CriteriaPriority = row.get("CriteriaPriority");
            RDFNode valueWithHighestWeight = row.get("valueWithHighestWeight");
            RDFNode valueWithLowestWeight = row.get("valueWithLowestWeight");

            Criteria c = new Criteria();
            c.setName(name.toString());
            c.setOwner(owner.toString());
            //must solve a problem of attached datatype here
            c.setCriteriaPrtiority(CriteriaPriority.toString());//.replace(Namespace, "").replace("^^http://www.w3.org/2001/XMLSchema#int", ""));
            c.setValueWithHighestWeight(valueWithHighestWeight.toString());//.replace(Namespace, "").replace("^^http://www.w3.org/2001/XMLSchema#int", ""));
            c.setValueWithLowestWeight(valueWithLowestWeight.toString());//.replace(Namespace, "").replace("^^http://www.w3.org/2001/XMLSchema#int", ""));

            if (!critereaList.contains(c)) {
                critereaList.add(c);
            }
        }
        qe.close();
    }

    private static double[][] createMainComparsion() {

        if (critereaList.size() <= 0) {
            return null;
        }

        double[][] mainComparsion = new double[critereaList.size()][critereaList.size()];
        for (int i = 0; i < critereaList.size(); i++) {
            mainComparsion[i][i] = 1;
        }

        for (int i = 0; i < critereaList.size(); i++) {
            for (int j = i + 1; j < critereaList.size(); j++) {
                double pi = Double.parseDouble(critereaList.get(i).getRefinedCriteriaPrtiority());
                double pj = Double.parseDouble(critereaList.get(j).getRefinedCriteriaPrtiority());

                double x = Math.abs(pi - pj);

                int judgment = 1;
                if (x < 2) {
                    judgment = 1;
                } else if (2 <= x && x < 4) {
                    judgment = 3;
                } else if (4 <= x && x < 6) {
                    judgment = 5;
                } else if (6 <= x && x < 8) {
                    judgment = 7;
                } else if (8 <= x) {
                    judgment = 9;
                }

                if (pi > pj) {
                    mainComparsion[i][j] = judgment;
                } else {
                    mainComparsion[i][j] = Math.pow(judgment, -1);
                }

                mainComparsion[j][i] = Math.pow(mainComparsion[i][j], -1);
            }
        }

        return mainComparsion;
    }

    //get the device object from the detectedDevices list through passing its real name.
    private static DeviceReference getDeviceFromList(String deviceName) throws CommunicationException {
        DeviceReference device = null;
        for (int i = 0; i < SearchClient.deviceList.size(); i++) {
            if (Data.getShortDeviceName(SearchClient.deviceList.get(i).getDevice()).startsWith(deviceName)) {
                device = SearchClient.deviceList.get(i);
            }
        }
        return device;
    }

    //get the service object from the detectedServices list through passing its real name.
    private static ServiceReference getServiceFromList(String serviceName, DeviceReference temporigin) throws CommunicationException {
        ServiceReference serviceNext = null;
        boolean found = false;
        Iterator serviceReferences = temporigin.getDevice().getServiceReferences(SecurityKey.EMPTY_KEY);
        while (serviceReferences.hasNext()) {
            serviceNext = (ServiceReference) serviceReferences.next();
            URI myURI = serviceNext.getServiceId();
            // substring after the last '/' or the entire String
            //String subString = myURI.getPath();
            //int index = subString.lastIndexOf('/') + 1;
            //subString = subString.substring(index);
            if (Data.getPureServiceName(serviceNext.getService()).equals((serviceName))) {
                found = true;
                break;
            }
        }
        if (found) {
            return serviceNext;
        } else {
            return null;
        }
    }

    public static void printMe(String msg, TimeInfo.status status) {

        System.err.println("doMigration" + "  " + msg);

        if (status != null) {
            executionTime.add(new TimeInfo(status));
        }
    }

    public static List<String> QueryCandidated(InfModel inf, String Var) throws CommunicationException {

        String queryString = "PREFIX  g:    <" + Namespace + ">\n"
                + "PREFIX  rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
                + "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
                + "\n"
                + "SELECT distinct ?x \n"
                + "WHERE\n"
                + "  { "
                + Var
                + "  }\n"
                + "";

        Query query = QueryFactory.create(queryString);
        QueryExecution qe = QueryExecutionFactory.create(query, inf);

        int i = 0;
        ResultSet results = qe.execSelect();
        List<String> tempList = new ArrayList<String>();
        while (results.hasNext()) {
            i++;
            QuerySolution row = results.nextSolution();

            RDFNode xNode = row.get("x");

            String xString = xNode.toString().replace(Namespace, "");

            if (!tempList.contains(xString)) {
                tempList.add(xString);
            }
        }
        qe.close();
        return tempList;
    }
}
