package com.ws4d.droidcommander;

import com.mohanned.migration.Data;
import com.mohanned.migration.gui.NewApplication;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ws4d.java.client.DefaultClient;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.types.SearchParameter;

import org.ws4d.java.security.SecurityKey;

/**
 * Sample client for searching devices and services.
 */
public class SearchClient extends DefaultClient {

    public static List<DeviceReference> deviceList = new ArrayList<>();

    public SearchClient() {
    }


    /*public void serviceFound(ServiceReference servRef, SearchParameter search) {
//		synchronized (TimerWork.detectedServices){
        try {
            Service service = servRef.getService();

            if (!ExploreActivity.detectedServices.contains(service)) {
                ExploreActivity.detectedServices.add(service);
//				TimerWork.detectedServices.notify();
            }

        } catch (CommunicationException e) {
            e.printStackTrace();
        }
//		}
    }*/
    public void deviceFound(DeviceReference devRef, SearchParameter search) {
        try {
            //		synchronized (TimerWork.detectedDevices){
            Device device = devRef.getDevice();
            //System.err.println("Found Provider: " + device.getFriendlyName(LocalizedString.LANGUAGE_EN));

            if (!deviceList.contains(device.getDeviceReference(SecurityKey.EMPTY_KEY))) {
                deviceList.add(device.getDeviceReference(SecurityKey.EMPTY_KEY));

                System.err.println(Data.getShortDeviceName(device) + " is found!");
                if (NewApplication.thatGUIForm != null) {
                    ((NewApplication) NewApplication.thatGUIForm).addDeviceToTreeList(device);
                }
            }

        } catch (CommunicationException ex) {
            Logger.getLogger(SearchClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}