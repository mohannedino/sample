/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ws4d.droidcommander;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.mohanned.migration.Configuration;
import java.io.IOException;
import java.io.StringReader;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.restlet.representation.Representation;
import org.restlet.resource.ClientResource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.ws4d.java.communication.CommunicationException;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Huho
 */
public class doctest {

    public static void main(String[] args) throws CommunicationException, ParserConfigurationException, SAXException, IOException {
        addResourceContextToModel("XProvider", getDeviceContext());
    }

    public static String getDeviceContext() throws CommunicationException, ParserConfigurationException, SAXException, IOException {

        String devName = "XProvider";
        String devIP = "192.168.56.102";
        String serviceName = "service2";

        String deviceContextServiceAPI = "http://" + devIP + ":8080/restdroid/getProviderContext/" + devName;
        ClientResource deviceRef = new ClientResource(deviceContextServiceAPI);
        Representation response = deviceRef.get();
        String deviceContext = response.getText();
        return deviceContext;

        //serviceContextAPI = "http://" + devIP + ":8080/" + serviceName + "/getServiceContext";
        //ClientResource serviceContextRef = new ClientResource(serviceContextAPI);
        //response = serviceContextRef.get();
        //String serviceContext = response.getText();
    }

    public static void addResourceContextToModel(String ResourceName, String XMLContext) throws ParserConfigurationException, SAXException, IOException {

        if (!XMLContext.contains("<?xml version=\"1.0\" encoding=\"utf-8\"?>")) {
            XMLContext = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + XMLContext;
        }
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(XMLContext));
        DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = dBuilder.parse(is);
        if (doc == null) {
            throw new RuntimeException("Mohanned Msg: " + ResourceName + " model is not a valid XML file!");
        }
        addnoPreferences(ResourceName, doc);
        addHasProperties(ResourceName, doc);
        addRules(doc);
    }

    private static void addnoPreferences(String ResourceName, Document doc) {
        String noPreferenceRulesValue = doc.getElementsByTagName("noPreferenceRules").item(0).getTextContent();
        addNoPreferenceRules(ResourceName, noPreferenceRulesValue);
    }

    private static void addHasProperties(String ResourceName, Document doc) {
        NodeList properties = doc.getElementsByTagName("hasProperty");

        for (int i = 0; i < properties.getLength(); i++) {
            {
                String propName, propType = null, propValue = null;

                Node NodeI = properties.item(i);
                System.err.println("NodeI =" + NodeI.getNodeName());

                for (int j = 0; j < NodeI.getChildNodes().getLength(); j++) {

                    Node NodeJ = NodeI.getChildNodes().item(j);

                    if (!"#text".equals(NodeJ.getNodeName())) {
                        System.err.println("NodeJ = " + NodeJ.getNodeName());
                        propName = NodeJ.getNodeName();
                        List<String> tempCriteriaList = new ArrayList<String>();

                        Element eElement = (Element) NodeJ.getChildNodes();
                        Element propertyValue = (Element) eElement.getElementsByTagName("propertyValue").item(0);
                        Element propertyType = (Element) eElement.getElementsByTagName("propertyType").item(0);

                        System.out.println("propertyValue: " + propertyValue.getTextContent());
                        System.out.println("propertyType: " + propertyType.getTextContent());

                        propType = propertyType.getTextContent();
                        propValue = propertyValue.getTextContent();

                        NodeList criteria = eElement.getElementsByTagName("criteria");
                        for (int k = 0; k < criteria.getLength(); k++) {
                            if (!"#text".equals(criteria.item(k).getTextContent())) {
                                System.err.println("criteria = " + criteria.item(k).getTextContent());
                                tempCriteriaList.add(criteria.item(k).getTextContent());

                            }
                        }
                        //add name of the property
                        if (propName != null && propType != null && propValue != null) {
                            addPropertyToModel(ResourceName, propName, propType, propValue, tempCriteriaList);
                        }
                        tempCriteriaList.clear();

                    }
                }
            }
        }
    }

    private static void addRules(Document doc) {
        for (int i = 0; i < doc.getElementsByTagName("rule").getLength(); i++) {
            String rule = doc.getElementsByTagName("rule").item(i).getTextContent();
            addRulesToModel(rule.trim());
        }
    }

    private static void addNoPreferenceRules(String ResouceName, String noPreferenceRulesValue) {
        System.err.println("Statment added :\n (NoPreferenceRules," + ResouceName + "): " + noPreferenceRulesValue);
    }

    private static void addPropertyToModel(String ResourceName, String propName, String propType, String propValue, List<String> tempCriteriaList) {
        if (propName != "" && propType != "" && propValue != "") {
            Resource PropertyName = ExploreActivity.model.getResource(ExploreActivity.Namespace + propName);

            Random rand = new Random();

            Individual InstancePropertyName = ExploreActivity.model.createIndividual(ExploreActivity.Namespace + PropertyName.getLocalName() + rand.nextInt(), PropertyName);

            System.err.println("PropertyInstance :\n (add PropertyName to the model for Resource" + ResourceName + "): " + propName + " called '" + InstancePropertyName.toString() + "'");

            Resource subject = ExploreActivity.model.getResource(ExploreActivity.Namespace + ResourceName);
            Property predicate = ExploreActivity.model.getProperty(ExploreActivity.Namespace + "hasProperty");
            Resource object = InstancePropertyName;

            Statement s = ResourceFactory.createStatement(subject, predicate, object);
            ExploreActivity.model.add(s);

            System.err.println("'" + InstancePropertyName.toString() + "' PropertyInstance \n attached to the Resource '" + ResourceName + "'):\n" + propName + "called " + InstancePropertyName.toString());

            Literal myType = ExploreActivity.model.createLiteral(propType);
            //should be typed literal
            //Literal myValue = model.createTypedLiteral(new Integer(propValue) );
            int myIntPropValue = Integer.parseInt(propValue);
            Literal myValue = ExploreActivity.model.createTypedLiteral(myIntPropValue);

            Property propertyType = ExploreActivity.model.getProperty(ExploreActivity.Namespace + "propertyType");
            Property propertyValue = ExploreActivity.model.getProperty(ExploreActivity.Namespace + "propertyValue");
            Property criteria = ExploreActivity.model.getProperty(ExploreActivity.Namespace + "criteria");

            ExploreActivity.model.add(InstancePropertyName, propertyType, myType);
            ExploreActivity.model.add(InstancePropertyName, propertyValue, myValue);
            System.err.println("Statment added an:\n" + InstancePropertyName.toString() + " , " + propertyType.toString() + " , " + myType.toString());
            System.err.println("Statment added an:\n" + InstancePropertyName.toString() + " , " + propertyValue.toString() + " , " + myValue.toString());

            for (int i = 0; i < tempCriteriaList.size(); i++) {
                Resource criteriaValue = ExploreActivity.model.getResource(tempCriteriaList.get(i).toString());
                ExploreActivity.model.add(InstancePropertyName, criteria, criteriaValue);
                System.err.println("Statment added an:\n" + InstancePropertyName.toString() + " , " + criteria.toString() + " , " + criteriaValue.toString());

            }
            tempCriteriaList.clear();

        }
    }

    private static void addRulesToModel(String line) {
        String rule = line.replace("{Rule}", "");
        //ExploreActivity.rules += "\n" + rule;

        Configuration.addRules(rule);

        System.err.println("Rule added: \n" + rule);
    }

}
