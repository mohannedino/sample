package move.devices;

class GPSPoint {
    float lat;
    float lng;
    String distance;

    public String getDistance() {
        return distance;
    }

    public GPSPoint(float newLat, float newLng) {
        setLat(newLat);
        setLng(newLng);
    }

    public static void setDistance(String distance) {
        distance = distance;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }

}
