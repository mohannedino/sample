package move.devices;


import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.data.Method;
import org.restlet.representation.Representation;
import org.restlet.resource.ClientResource;

import java.io.IOException;

/**
 * Created by Huho on 4/29/2017.
 */

public class miniRestdroid {


    public static void restletServiceUpdateV3(String IP,GPSPoint point, float speed, GPSPoint center) {
        String msg = "Posted";
        String api = "";
        if (point != null) {
            try {
                // Initialize the resource proxy.
                api = "http://" + IP + ":8080/restdroid/position/post/lng/" + point.getLng() + "/lat/" +
                        point.getLat() + "/speed/" + speed +
                        "/centerLng/" + center.getLng() +
                        "/centerLat/" + center.getLat();
                ;
                ClientResource post = new ClientResource(Method.POST, api);
                Representation response = post.post("");

            } catch (Exception ex) {
                msg = "Error";
            } finally {
            }


        }
    }


    public static JSONObject getLngLatSpeed(String devIP) throws IOException, JSONException {
        JSONObject x = getDeviceLngLatSpeedCenter(devIP);
        JSONObject lngLatSpeed = new JSONObject();
        lngLatSpeed.put("lng", x.get("lng"));
        lngLatSpeed.put("lat", x.get("lat"));
        lngLatSpeed.put("speed", x.get("speed"));
        return lngLatSpeed;
    }

    public static JSONObject getDeviceLngLatSpeedCenter(String devIP) {
        String lngLat = "";
        JSONObject obj = null;
        String api = "http://" + devIP + ":8080/restdroid/position/get";
        ClientResource client = new ClientResource(Method.GET, api);
        try {
            Representation response = client.get();

            lngLat = response.getText();

            obj = new JSONObject(lngLat);
            //Object jsonMe = JSON.parse(lngLat);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();

        }
        return obj;
    }

    

    public static JSONObject getInterestCenter(String devIP) throws IOException, JSONException {
        JSONObject x = getDeviceLngLatSpeedCenter(devIP);
        JSONObject center = new JSONObject();
        center.put("clng", x.get("clng"));
        center.put("clat", x.get("clat"));
        return center;
    }

}
