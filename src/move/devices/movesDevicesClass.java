/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package move.devices;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import test.point;
import test.setPositionsAndSpeeds;

/**
 *
 * @author Huho
 */
public class movesDevicesClass {

    static ArrayList<point> pointList = new ArrayList<>();
    static String IP1 = "192.168.0.121";
    static String IP2 = "192.168.0.122";
    static String IP5 = "192.168.0.125";

    static int positionD1 = 0;
    static int positionD2 = 0;
    static int positionD5 = 0;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        // TODO code application logic here
        //JSONArray a = (JSONArray) parser.parse(new FileReader("dots.txt"));
        
        setPositionsAndSpeeds.main(args);
        pointList = setPositionsAndSpeeds.points;
        moveDevices();

       
    }

    public static int getRandomSpeed() {
        int randomNum = 10 + (int) (Math.random() * 40);
        return randomNum;
    }

    static private void moveDevices() {

        //set position
        //device1 = 100 M. point 2.
        //device2 = 200 M. point 6.
        //device5 = 300 M. point 10.
        positionD1 = 2;
        positionD2 = 6;
        positionD5 = 10;

        Thread timeD1 = new Thread(new Runnable() {
            @Override
            public void run() {

                while (true) {
                    int speed = getRandomSpeed();
                    //change position and speed
                    GPSPoint point = getNextPoint(positionD1);
                    positionD1++;
                    ///urlll;/////
                    miniRestdroid.restletServiceUpdateV3(IP1, point, speed, new GPSPoint(0, 0));

                    //how many seconds to wait till moving to the next points == timer duration.
                    //1- with 10KM/H time is 512 sec/
                    //2- with speed .. time will be interval
                    //int intervalD2 = (speed * 512 / 10) * 1000;
                    double intervalD2 = (8.533f * 10 / speed) * 1000;
                    try {
                        Thread.sleep((int) intervalD2);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        Thread timeD2 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    int speed = getRandomSpeed();
                    //change position and speed
                    GPSPoint point = getNextPoint(positionD2);
                    positionD2++;
                    ///urlll;/////
                    miniRestdroid.restletServiceUpdateV3(IP2, point, speed, new GPSPoint(0, 0));

                    //how many seconds to wait till moving to the next points == timer duration.
                    //1- with 10KM/H time is 512 sec/ for 60 points -- each point switch is 512/60 == 8.533 sec
                    //2- with speed .. time will be interval ---
                    //int intervalD3 = (speed * 512 / 10) * 1000;
                    double intervalD3 = (8.533f * 10 / speed) * 1000;
                    try {
                        Thread.sleep((int) intervalD3);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        Thread timeD5 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    int speed = getRandomSpeed();
                    //change position and speed
                    GPSPoint point = getNextPoint(positionD5);
                    positionD5++;
                    ///urlll;/////
                    miniRestdroid.restletServiceUpdateV3(IP5, point, speed, new GPSPoint(0, 0));

                    //how many seconds to wait till moving to the next points == timer duration.
                    //1- with 10KM/H time is 512 sec/
                    //2- with speed .. time will be interval
                    double intervalD5 = (8.533f * 10 / speed) * 1000;
                    try {
                        Thread.sleep((int) intervalD5);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        timeD1.start();
        timeD2.start();
        timeD5.start();
    }

    static private GPSPoint getNextPoint(int index) {
        if (index < pointList.size() - 1) {
            return 
                    new GPSPoint(pointList.get(index + 1).getLat(), pointList.get(index + 1).getLng());
        } else {
            return new GPSPoint(pointList.get(index - 1).getLat(), pointList.get(index - 1).getLng());
        }
    }
}
