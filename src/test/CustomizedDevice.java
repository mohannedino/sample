/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import com.mohanned.migration.Configuration;
import java.io.Serializable;
import org.ws4d.java.service.DefaultDevice;
import org.ws4d.java.types.LocalizedString;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.ScopeSet;

/**
 *
 * @author Huho
 */
public class CustomizedDevice extends DefaultDevice implements Serializable{

    ScopeSet scopeList = null;

    public CustomizedDevice() {
        super();
        // set PortType
        this.setPortTypes(new QNameSet(new QName("Provider", Configuration.ws4dNamespace)));

        addManufacturer(LocalizedString.LANGUAGE_EN, "VUT - Univ. Brno.");
        addModelName(LocalizedString.LANGUAGE_EN, "Provider");
        setModelNumber(System.getProperty("os.name"));
        setFirmwareVersion("v1.0");

        scopeList = new ScopeSet(new String[]{"urn:" + System.getProperty("os.name")});
        setScopes(scopeList);

    }
    
}
