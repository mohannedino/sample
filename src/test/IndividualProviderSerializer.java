/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.mohanned.migration.IndividualProvider;
import java.lang.reflect.Type;


/**
 *
 * @author Huho
 */
public class IndividualProviderSerializer implements JsonSerializer<IndividualProvider>
{
    public JsonElement serialize(IndividualProvider t, Type type, 
                                 JsonSerializationContext jsc)
    {
        JsonObject jo = new JsonObject();
        jo.addProperty("", t.getDefaultNamespace());
        // etc for all the publicly available getters
        // for the information you're interested in
        // ...
        jo.addProperty("isRunning",t.isRunning());
           // jo.addProperty("isRunning",t.start());
        //jo.addProperty("PortTypes", t.get());


        //addManufacturer(LocalizedString.LANGUAGE_EN, "VUT - Univ. Brno.");
        //addModelName(LocalizedString.LANGUAGE_EN, "Provider");
        //setModelNumber(System.getProperty("os.name"));
        //setFirmwareVersion("v1.0");

        //scopeList = new ScopeSet(new String[]{"urn:" + System.getProperty("os.name")});
        //setScopes(scopeList);

        return jo;
    }

}
